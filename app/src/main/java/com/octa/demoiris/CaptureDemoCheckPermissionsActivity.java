package com.octa.demoiris;

import android.content.Intent;

import com.deltaid.sample_utils.CheckPermissionsActivity;


public class CaptureDemoCheckPermissionsActivity extends CheckPermissionsActivity {
  @Override
  public void startNewActivity() {
    startActivity(new Intent(this, MainActivity.class));
  }

  @Override
  public String getAppName() {
    return getString(R.string.app_name);
  }
}
