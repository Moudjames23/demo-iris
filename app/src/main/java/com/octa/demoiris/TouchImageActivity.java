package com.octa.demoiris;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.os.Bundle;

public class TouchImageActivity extends Activity {
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_touch_image);

    TouchImageView touchImgVu = findViewById(R.id.touchImgVu);

    Intent intent = getIntent();
    int imgWidth = intent.getIntExtra("IMAGE_WIDTH", 0);
    int imgHeight = intent.getIntExtra("IMAGE_HEIGHT", 0);
    byte [] pixels = intent.getByteArrayExtra("IMAGE_DATA");

    if (pixels != null) {
      Bitmap bitmap = Bitmap.createBitmap(imgWidth, imgHeight, Config.ARGB_8888);
      int imgLeft[] = new int[imgWidth * imgHeight];
      for (int i = 0; i < imgWidth * imgHeight; i++) {
        int p = pixels[i] & 0xff;
        imgLeft[i] = 0xff000000 | (p << 16) | (p << 8) | p;
      }
      bitmap.setPixels(imgLeft, 0, imgWidth, 0, 0, imgWidth, imgHeight);
      touchImgVu.setImageBitmap(bitmap);
    }
  }
}
