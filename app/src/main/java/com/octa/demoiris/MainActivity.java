package com.octa.demoiris;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.text.format.Time;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private final String TAG = "main_tag";

    Button captureBtn;
    Button cancelBtn;
    int imgWidth, imgHeight;
    String numEye = "two";
    byte [] pixelsEye1 = null; // left eye if 2 eye capture
    byte [] pixelsEye2 = null; // right eye if 2 eye capture
    byte [] isoEye1 = null;
    byte [] isoEye2 = null;
    Bitmap bitmapEye1 = null;
    Bitmap bitmapEye2 = null;
    LinearLayout touchImgVuContainer;
    ImageView eye1ImgVu, eye2ImgVu;
    CountDownTimer durTimer;
    final static String extStorageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getPath();
    File imgDir = new File(extStorageDirectory, "CaptureDemo/Kind7Images");
    final int CAPTURE_REQUEST = 1;
    final static int [] capture_settings = new int[] { 800, 12, 30, 50, 0, 255, 0, 255 };
    private long captureTime, kind7Time;
    private TextView timeText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        timeText = (TextView) findViewById(R.id.timeText);

        captureBtn = (Button) findViewById(R.id.capture_btn);
        captureBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                captureBtn.setEnabled(false);
                pixelsEye1 = null;
                pixelsEye2 = null;
                eye1ImgVu.setImageBitmap(null);
                eye2ImgVu.setImageBitmap(null);
                startCapture("front");
                captureBtn.setVisibility(View.GONE);
                cancelBtn.setVisibility(View.VISIBLE);
                timeText.setText(String.format("CaptureTime:    ms  Kind7Creation:   ms"));
            }
        });

        cancelBtn = (Button) findViewById(R.id.cancel_btn);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent("com.deltaid.iris_mobile.cancel");
                sendBroadcast(intent);
            }
        });

        RadioButton oneEyeBtn = (RadioButton) findViewById(R.id.oneEyeBtn);
        RadioButton twoEyeBtn = (RadioButton) findViewById(R.id.twoEyeBtn);
        View.OnClickListener onRadBtnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                numEye = (String) v.getTag();
            }
        };
        oneEyeBtn.setOnClickListener(onRadBtnClickListener);
        twoEyeBtn.setOnClickListener(onRadBtnClickListener);

        touchImgVuContainer = (LinearLayout) findViewById(R.id.touchImgVuContainer);
        View.OnTouchListener imgTouchListener = new View.OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                ImageView imgVu = (ImageView) arg0;
                Intent intent = new Intent(getApplicationContext(), TouchImageActivity.class);
                intent.putExtra("IMAGE_WIDTH", imgWidth);
                intent.putExtra("IMAGE_HEIGHT", imgHeight);
                if (imgVu.equals(eye1ImgVu)) {
                    if (pixelsEye1 != null) {
                        intent.putExtra("IMAGE_DATA", pixelsEye1);
                    }
                } else {
                    if (pixelsEye2 != null) {
                        intent.putExtra("IMAGE_DATA", pixelsEye2);
                    }
                }
                startActivity(intent);
                return false;
            }

        };
        eye1ImgVu = (ImageView) findViewById(R.id.leftEyeImgVu);
        eye2ImgVu = (ImageView) findViewById(R.id.rightEyeImgVu);
        eye1ImgVu.setOnTouchListener(imgTouchListener);
        eye2ImgVu.setOnTouchListener(imgTouchListener);


    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        captureBtn.setVisibility(View.VISIBLE);
        cancelBtn.setVisibility(View.GONE);
        captureBtn.setEnabled(true);
        if (requestCode == CAPTURE_REQUEST) {
            if (resultCode == RESULT_OK) {
                Log.d(TAG, "Result OK: ");
                captureTime = data.getLongExtra("CAPTURE_TIME", 0L);
                kind7Time = data.getLongExtra("KIND7_TIME", 0L);
                timeText.setText(String.format("CaptureTime:  %d  ms  Kind7Creation:  %d ms", captureTime, kind7Time));
                pixelsEye1 = data.getByteArrayExtra("PIXELS_EYE1");
                isoEye1 = data.getByteArrayExtra("ISO_RECORD_EYE1");
                pixelsEye2 = data.getByteArrayExtra("PIXELS_EYE2");
                isoEye2 = data.getByteArrayExtra("ISO_RECORD_EYE2");
                if (pixelsEye1 != null || pixelsEye2 != null) {
                    Log.d(TAG, "data not null: ");
                    imgWidth = data.getIntExtra("IMAGE_WIDTH", 0);
                    imgHeight = data.getIntExtra("IMAGE_HEIGHT", 0);
                    displayCaptureImage();

                    Log.d(TAG, "Taille 1: " +(pixelsEye1.length / 1024)+ " KB");
                    Log.d(TAG, "Taille 2: " +(pixelsEye2.length / 1024)+ " KB");

                   // eye1ImgVu.setImageBitmap(byteArrayToBitmap(pixelsEye1));
                    //eye2ImgVu.setImageBitmap(byteArrayToBitmap(pixelsEye2));

                } else if (pixelsEye1 == null && pixelsEye2 == null) {
                    Toast.makeText(this, "Capture returned resultCode = " + resultCode, Toast.LENGTH_LONG).show();
                }
            } else if (resultCode == RESULT_CANCELED) {
                String reason = null;
                if (data != null) {
                    reason = data.getStringExtra("REASON");
                }
                if (reason == null) {
                    reason = "cancelled";
                }
                Toast.makeText(this, "Capture failed, reason: " + reason, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Capture failed, resultcode: " + resultCode, Toast.LENGTH_LONG).show();
            }
        }
    }

    public  Bitmap byteArrayToBitmap(byte[] byteArray) {
        return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

    }

    void startCapture(String camName) {
        captureBtn.setEnabled(false);
        Intent captureIntent = new Intent();
        captureIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        captureIntent.setAction("com.deltaid.iris_mobile.capture");
        captureIntent.putExtra("NUM_EYES", numEye.equals("two") ? 2 : 1);
        captureIntent.putExtra("ISO_IMAGE_TYPE", "kind7");
        captureIntent.putExtra("CAPTURE_SETTINGS", capture_settings);
        try {
            startActivityForResult(captureIntent, CAPTURE_REQUEST);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(getApplicationContext(), "Could not start CaptureActivity.", Toast.LENGTH_LONG).show();
        }
    }

    void displayCaptureImage() {
        //File extStorageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        //File imgDir = new File(extStorageDirectory, "Demo/Kind7Images");
        imgDir.mkdirs();
        String imgBase = new File(imgDir, getNowString()).toString();
        touchImgVuContainer.setVisibility(View.VISIBLE);
        String ts = getNowString();
        imgDir.mkdirs();
        if (pixelsEye1 != null) {
            if (bitmapEye1 == null) {
                bitmapEye1 = Bitmap.createBitmap(imgWidth, imgHeight, Bitmap.Config.ARGB_8888);
            }
            int imgLeft[] = new int[imgWidth * imgHeight];
            for (int i = 0; i < imgWidth * imgHeight; i++) {
                int p = pixelsEye1[i] & 0xff;
                imgLeft[i] = 0xff000000 | (p << 16) | (p << 8) | p;
            }
            bitmapEye1.setPixels(imgLeft, 0, imgWidth, 0, 0, imgWidth, imgHeight);
            eye1ImgVu.setImageBitmap(bitmapEye1);
            saveImage(imgBase + "_L.png", bitmapEye1);
        }
        if (pixelsEye2 == null) {
            eye2ImgVu.setVisibility(View.GONE);
        } else {
            eye2ImgVu.setVisibility(View.VISIBLE);
            int imgRight[] = new int[imgWidth * imgHeight];
            if (bitmapEye2 == null) {
                bitmapEye2 = Bitmap.createBitmap(imgWidth, imgHeight, Bitmap.Config.ARGB_8888);
            }
            for (int i = 0; i < imgWidth * imgHeight; i++) {
                int p = pixelsEye2[i] & 0xff;
                imgRight[i] = 0xff000000 | (p << 16) | (p << 8) | p;
            }
            bitmapEye2.setPixels(imgRight, 0, imgWidth, 0, 0, imgWidth, imgHeight);
            eye2ImgVu.setVisibility(View.VISIBLE);
            eye2ImgVu.setImageBitmap(bitmapEye2);
            saveImage(imgBase + "_R.png", bitmapEye2);
        }
    }

    private static String getNowString() {
        Time timeVideoStarted = new Time();
        timeVideoStarted.setToNow();
        return timeVideoStarted.format2445();
    }

    void saveImage(String file, Bitmap bmp) {
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}