LOCAL_PATH := $(call my-dir)

ifeq ($(filter $(IRM_STATIC_ONLY),$(TARGET_ARCH_ABI)),)

include $(CLEAR_VARS)

LOCAL_MODULE    := sample_utils_jni
LOCAL_SRC_FILES := sample_utils_jni.cpp
LOCAL_ARM_MODE := thumb
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)

#the following -Wl,--kill-at switch is needed for JNI libraries only for mingw32
ifneq ($(filter $(TARGET_ARCH_ABI), i686-mingw),)
LOCAL_LDFLAGS +=   -Wl,--kill-at
LOCAL_CFLAGS +=    -static-libstdc++ -static
LOCAL_LDFLAGS +=   -static-libstdc++ -static
endif

ifneq ($(filter $(TARGET_ARCH_ABI), x86_64-mingw),)
LOCAL_CFLAGS +=    -static-libstdc++ -static
LOCAL_LDFLAGS +=   -static-libstdc++ -static
endif

include $(BUILD_SHARED_LIBRARY)

endif
