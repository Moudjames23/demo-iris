#include <string.h>
#include <stdint.h>
#include "com_deltaid_sample_utils_util_ConfigFileUtils.h"
#include "sut_jarray.h"

#define SUT_STATUS_OK                       (0x00000000)
#define SUT_STATUS_PENDING                  (0x0000000A)
#define SUT_STATUS_STEP_COMPLETED           (0x0004001A)
#define SUT_STATUS_NOT_UPDATED              (0x00040002)

#define SUT_ERR_INVALID_PARAM               (0x80000003)
#define SUT_ERR_OUTOFMEMORY                 (0x80000002)
#define SUT_ERR_NOT_IMPLEMENTED             (0x80000001)
#define SUT_ERR_FAIL                        (0x80000008)
#define SUT_ERR_UNAVAILABLE                 (0x800401E3)

JNIEXPORT jint JNICALL Java_com_deltaid_sample_1utils_util_ConfigFileUtils_convert_1to_1argb(JNIEnv *env, jclass cls, jbyteArray jpixels_in, jint stride_in, jint xoffset_in, jint yoffset_in, jintArray jpixels_out, jint width_out, jint height_out, jboolean isRaw10) {
  if (jpixels_in == NULL) {
    return SUT_ERR_INVALID_PARAM;
  }
  if (jpixels_out == NULL) {
    return SUT_ERR_INVALID_PARAM;
  }
  JByteArray pixels_in(env, jpixels_in);
  JIntArray pixels_out(env, jpixels_out);
  if (pixels_out.size() < (width_out * height_out)) {
    return SUT_ERR_INVALID_PARAM;
  }
  uint8_t *pin = pixels_in + yoffset_in * stride_in + xoffset_in;
  int32_t *pout = pixels_out;
  int d_stride_width = stride_in - width_out;

  if (isRaw10) {
    pin += (xoffset_in >> 2); // assumption: xoffset_in % 4 == 0
    d_stride_width -= width_out >> 2; // assumption: width_out % 4 == 0
    for (int y = 0; y < height_out; ++y, pin += d_stride_width) {
      for (int x = 0; x < width_out; ++x, ++pout, pin += (x & 3) ? 1 : 2) {
        int val = *pin;
        *pout = val | (val << 8) | (val << 16) | 0xff000000;
      }
    }
  } else {
    //convert grayscale to argb
    for (int y = 0; y < height_out; ++y, pin += d_stride_width) {
      for (int x = 0; x < width_out; ++x, ++pout, ++pin) {
        int val = *pin;
        *pout = val | (val << 8) | (val << 16) | 0xff000000;
      }
    }
  }

  return SUT_STATUS_OK;
}

/*
 * Class:     com_deltaid_sample_utils_util_ConfigFileUtils
 * Method:    convert_to_argb_map_range
 * Signature: ([BIII[IIIZIIIII)I
 */
JNIEXPORT jint JNICALL Java_com_deltaid_sample_1utils_util_ConfigFileUtils_convert_1to_1argb_1map_1range
    (JNIEnv *env, jclass cls, jbyteArray jpixels_in, jint stride_in, jint xoffset_in, jint yoffset_in, jintArray jpixels_out, jint width_out, jint height_out, jboolean isRaw10, jint range_min_pixel, jint range_max_pixel, jint red_val, jint green_val, jint blue_val) {
  int red_map[256], green_map[256], blue_map[256];
  if (jpixels_in == NULL) {
    return SUT_ERR_INVALID_PARAM;
  }
  if (jpixels_out == NULL) {
    return SUT_ERR_INVALID_PARAM;
  }
  if (range_min_pixel > 255 || range_min_pixel < 0 || range_max_pixel < 0 || range_max_pixel > 255) {
    return SUT_ERR_INVALID_PARAM;
  }
  if (range_min_pixel > range_max_pixel) {
    return SUT_ERR_INVALID_PARAM;
  }
  JByteArray pixels_in(env, jpixels_in);
  JIntArray pixels_out(env, jpixels_out);
  if (pixels_out.size() < (width_out * height_out)) {
    return SUT_ERR_INVALID_PARAM;
  }
  uint8_t *pin = pixels_in + yoffset_in * stride_in + xoffset_in;
  int32_t *pout = pixels_out;
  int d_stride_width = stride_in - width_out;
  for (int i = 0; i < range_min_pixel; i++) {
    red_map[i] = green_map[i] = blue_map[i] = i;
  }
  for(int i = range_min_pixel; i <= range_max_pixel; i++) {
    red_map[i] = red_val;
    green_map[i] = green_val;
    blue_map[i] = blue_val;
  }
  for (int i = range_max_pixel + 1; i < 256; i++) {
    red_map[i] = green_map[i] = blue_map[i] = i;
  }

  if (isRaw10) {
    pin += (xoffset_in >> 2); // assumption: xoffset_in % 4 == 0
    d_stride_width -= width_out >> 2; // assumption: width_out % 4 == 0
    for (int y = 0; y < height_out; ++y, pin += d_stride_width) {
      for (int x = 0; x < width_out; ++x, ++pout, pin += (x & 3) ? 1 : 2) {
        int val = *pin;
        *pout = red_map[val] | (green_map[val] << 8) | (blue_map[val] << 16) | 0xff000000;
      }
    }
  } else {
    //convert grayscale to argb
    for (int y = 0; y < height_out; ++y, pin += d_stride_width) {
      for (int x = 0; x < width_out; ++x, ++pout, ++pin) {
        int val = *pin;
        *pout = red_map[val] | (green_map[val] << 8) | (blue_map[val] << 16) | 0xff000000;
      }
    }
  }

  return SUT_STATUS_OK;
}

JNIEXPORT jint JNICALL Java_com_deltaid_sample_1utils_util_ConfigFileUtils_raw10_1to_18bpp_1in_1place(JNIEnv *env, jclass cls, jbyteArray jpixels, jint stride, jint width, jint height) {
  if (jpixels == NULL) {
    return SUT_ERR_INVALID_PARAM;
  }
  JByteArray pixels(env, jpixels);
  if (pixels.size() < (width * height)) {
    return SUT_ERR_INVALID_PARAM;
  }
  uint8_t *pout = pixels;
  const uint8_t *pin = pout;
  int d_stride_width = stride - (width + (width >> 2));

  for (int y = 0; y < height; y++, pin += d_stride_width) {
    for (int x = 0; x < width; x++, ++pout, pin += (x & 3) ? 1 : 2) {
      *pout = *pin;
    }
  }
  return SUT_STATUS_OK;
}
