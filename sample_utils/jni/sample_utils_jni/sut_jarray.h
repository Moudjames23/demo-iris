/*
 * jbytearray.h
 *
 */

#ifndef TML_JARRAY_H_
#define TML_JARRAY_H_

#include <jni.h>

template <class JT, class JTA, class T> struct JArray {
  JArray(JNIEnv * env, JTA array)
    : m_env(env), m_jarray(array), m_array(0)
  {
    if (m_jarray) get();
  }

  ~JArray() {
    if (m_array) release();
  }

  void get();
  void release();

  operator T * () { return (T *) m_array; }
  JTA detach() {
    if (m_array) {
      release();
      m_array = NULL;
    }
    return m_jarray;
  }
  jsize size() const { return m_array ? m_env->GetArrayLength(m_jarray) : 0; }
protected:
  JNIEnv * m_env;
  JT * m_array;
  JTA m_jarray;
};

typedef JArray<jbyte, jbyteArray, unsigned char> JByteArray;
template<> inline void JByteArray::get() {
  m_array = m_env->GetByteArrayElements(m_jarray, NULL);
}
template<> inline void JByteArray::release() {
  m_env->ReleaseByteArrayElements(m_jarray, m_array, 0);
}

typedef JArray<jbyte, jbyteArray, const unsigned char> JConstByteArray;
template<> inline void JConstByteArray::get() {
  m_array = m_env->GetByteArrayElements(m_jarray, NULL);
}
template<> inline void JConstByteArray::release() {
  m_env->ReleaseByteArrayElements(m_jarray, m_array, JNI_ABORT);
}


typedef JArray<jint, jintArray, int32_t> JIntArray;
template<> inline void JIntArray::get() {
  m_array = m_env->GetIntArrayElements(m_jarray, NULL);
}
template<> inline void JIntArray::release() {
  m_env->ReleaseIntArrayElements(m_jarray, m_array, 0);
}

typedef JArray<jint, jintArray, uint32_t> JUIntArray;
template<> inline void JUIntArray::get() {
  m_array = m_env->GetIntArrayElements(m_jarray, NULL);
}
template<> inline void JUIntArray::release() {
  m_env->ReleaseIntArrayElements(m_jarray, m_array, 0);
}

typedef JArray<jlong, jlongArray, int64_t> JLongArray;
template<> inline void JLongArray::get() {
  m_array = m_env->GetLongArrayElements(m_jarray, NULL);
}
template<> inline void JLongArray::release() {
  m_env->ReleaseLongArrayElements(m_jarray, m_array, 0);
}

typedef JArray<jlong, jlongArray, uint64_t> JULongArray;
template<> inline void JULongArray::get() {
  m_array = m_env->GetLongArrayElements(m_jarray, NULL);
}
template<> inline void JULongArray::release() {
  m_env->ReleaseLongArrayElements(m_jarray, m_array, 0);
}


#endif /* TML_JARRAY_H_ */
