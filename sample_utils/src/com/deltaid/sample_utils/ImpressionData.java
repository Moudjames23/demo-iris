package com.deltaid.sample_utils;

import java.util.ArrayList;

/**
 * Created by venkynarayanan on 2/1/18.
 */

public class ImpressionData {
  public ArrayList<String>     imprId = new ArrayList<>();  // inserted in template/video file names
  public ArrayList<Integer>    led = new ArrayList<>(); // led values to set
  public ArrayList<Integer>     expMult = new ArrayList<>();
  public int                   distance = -1; // nominal resolution to be used
  public String                deviceClassName = null; // device to be used. Can be null
  public float                 previewZoom = 1.0f;

}
