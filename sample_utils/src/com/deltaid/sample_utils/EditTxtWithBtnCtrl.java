package com.deltaid.sample_utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class EditTxtWithBtnCtrl extends LinearLayout implements ExtendedEditText.OnEditingDoneListener {

  private String labelTxt, defaultOpVal;
  private ExtendedEditText opTxt;
  private TextWatcher opTxtWatcher;
  private Button opPlus, opMinus;
  private int minOpVal, maxOpVal;
  private Toast toast;
  private OnEditCtrlDoneListener doneListener;
  final private static int INVALID_VALUE = -1;
  final private static int REQ_EDITTEXT_LENGTH = 2;
  final private static int MAX_EDITEXT_LENGTH = REQ_EDITTEXT_LENGTH + 1;

  public EditTxtWithBtnCtrl(Context context, AttributeSet attrs) {
    super(context, attrs);

    TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.EditTxtWithBtnCtrl);
    labelTxt = a.getString(R.styleable.EditTxtWithBtnCtrl_labelText);
    defaultOpVal = a.getString(R.styleable.EditTxtWithBtnCtrl_defaultVal);
    minOpVal = Integer.parseInt(a.getString(R.styleable.EditTxtWithBtnCtrl_minVal));
    maxOpVal = Integer.parseInt(a.getString(R.styleable.EditTxtWithBtnCtrl_maxVal));
    a.recycle();

    setOrientation(LinearLayout.HORIZONTAL);
    setGravity(Gravity.CENTER_VERTICAL);

    toast = Toast.makeText(context, "", Toast.LENGTH_SHORT);

    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    inflater.inflate(R.layout.edittxt_with_btn_ctrl, this, true);
  }

  @Override
  protected void onFinishInflate() {
    super.onFinishInflate();

    TextView opLbl = (TextView)findViewById(R.id.op_lbl);
    opLbl.setText(labelTxt);

    opTxt = (ExtendedEditText)findViewById(R.id.op_txt);
    opTxt.setOnEditingDoneListener(this);
    opTxt.setText(defaultOpVal);

    opPlus = (Button)findViewById(R.id.op_plus);
    opMinus = (Button)findViewById(R.id.op_minus);

    setCallbacks();

  }

  public void hidePluMinusCtrls() {
    opPlus.setVisibility(View.GONE);
    opMinus.setVisibility(View.GONE);
  }

  public void setUneditable() {
    opTxt.setFocusable(false);
    opTxt.setFocusableInTouchMode(false);
    opTxt.setClickable(false);
  }

  public void setMinOpVal(int minVal) {
    this.minOpVal = minVal;
  }

  public void setMaxOpVal(int maxVal) {
    this.maxOpVal = maxVal;
  }

  private void setCallbacks() {
    opPlus.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        handlePlusMinus(true);
      }
    });

    opMinus.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        handlePlusMinus(false);
      }
    });

    opTxtWatcher = new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
      }

      @Override
      public void afterTextChanged(Editable s) {
        int iLen = s.length();
        if (iLen > REQ_EDITTEXT_LENGTH) {
          handleNumberEntry(opTxt, opTxtWatcher, s);
          return;
        }
      }
    };
    opTxt.addTextChangedListener(opTxtWatcher);
  }

  private void handlePlusMinus(boolean plus) {
    String sval = opTxt.getText().toString();
    int ival = INVALID_VALUE;
    if (sval != null && sval.length() > 0) {
      ival = Integer.parseInt(sval);
    }
    ival += (plus ? 1 : -1);
    if (ival > maxOpVal) {
      ival = maxOpVal;
    } else if (ival < minOpVal) {
      ival = minOpVal;
    }
    opTxt.setText(String.valueOf(ival));
    handleExit();
  }

  private void handleExit() {
    String sval = opTxt.getText().toString();
    boolean retval = checkRange(sval, minOpVal, maxOpVal);
    if (doneListener != null) {
      doneListener.onEditCtrlDone(this, retval);
    }
  }

  private void handleNumberEntry(EditText et, TextWatcher tw, Editable s) {
    String val;
    int cPos = et.getSelectionStart();
    et.removeTextChangedListener(tw);
    if (cPos == MAX_EDITEXT_LENGTH) {
      val = s.toString().substring(1, MAX_EDITEXT_LENGTH);
      et.setText("" + val);
      et.setSelection(0);
    } else {
      val = s.toString().substring(0, MAX_EDITEXT_LENGTH - 1);
      et.setText("" + val);
      et.setSelection(cPos);
    }
    et.addTextChangedListener(tw);
  }

  private boolean checkRange(String val, int min, int max) {
    int ival = -1;
    try {
      ival = Integer.parseInt(val);
    } catch (NumberFormatException nfe) {
    }
    if(val == null || val.length() == 0 || ival < min || ival > max) {
      toast.setText("Enter value between " + min + " and " + max + " for " + labelTxt);
      toast.show();
      return false;
    }
    return true;
  }

  public void onEditingDone(ExtendedEditText eet) {
    handleExit();
  }

  public void setValue(int value) {
    opTxt.setText(String.valueOf(value));
  }

  public int getValue() {
    String sval = opTxt.getText().toString();
    return (sval == null || sval.length() == 0) ? INVALID_VALUE : Integer.parseInt(sval);
  }

  public void setOnEditCtrlDoneListener(OnEditCtrlDoneListener listener) {
    doneListener = listener;
  }

  public interface OnEditCtrlDoneListener {
    public void onEditCtrlDone(EditTxtWithBtnCtrl etwbc, boolean valid);
  }

}
