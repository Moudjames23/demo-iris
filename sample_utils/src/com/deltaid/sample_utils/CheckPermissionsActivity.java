package com.deltaid.sample_utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.Toast;

abstract public class CheckPermissionsActivity extends Activity {

  private static int REQUEST_PERMISSION_CODE = 1;

  private int indexPermissionCamera;
  private int indexPermissionIris;
  private int indexPermissionWrite;
  private int indexPermissionRead;
  private boolean requestCameraPermission;
  private boolean requestIrisPermission;
  private boolean requestStoragePermission;
  private int numPermissionsToRequest;
  private boolean hasCameraPermission;
  private boolean hasIrisPermission;
  private boolean hasStoragePermission;
  private boolean permissionDenied;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public void onResume() {
    super.onResume();
    if (!permissionDenied) {
      numPermissionsToRequest = 0;
      checkPermissions();
    } else {
      permissionDenied = false;
    }
  }

  private void checkPermissions() {
    checkCameraPermission();
    checkIrisPermission();
    checkReadWritePermission();
    if (numPermissionsToRequest != 0) {
      buildPermissionsRequest();
    } else {
      handleSuccess();
    }
  }

  @TargetApi(23)
  private void checkCameraPermission() {
    if (checkSelfPermission(Manifest.permission.CAMERA)
        != PackageManager.PERMISSION_GRANTED) {
      numPermissionsToRequest++;
      requestCameraPermission = true;
    } else {
      hasCameraPermission = true;
    }
  }

  @TargetApi(23)
  private void checkIrisPermission() {
    if (checkSelfPermission("com.id2mp.permissions.IRIS")
        != PackageManager.PERMISSION_GRANTED) {
      numPermissionsToRequest++;
      requestIrisPermission = true;
    } else {
      hasIrisPermission = true;
    }
  }

  @TargetApi(23)
  private void checkReadWritePermission() {
    if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        != PackageManager.PERMISSION_GRANTED ||
        checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED) {
      numPermissionsToRequest = numPermissionsToRequest + 2;
      requestStoragePermission = true;
    } else {
      hasStoragePermission = true;
    }
  }

  @TargetApi(23)
  private void buildPermissionsRequest() {
    String[] permissionsToRequest = new String[numPermissionsToRequest];
    int permissionsRequestIndex = 0;

    if (requestCameraPermission) {
      permissionsToRequest[permissionsRequestIndex] = Manifest.permission.CAMERA;
      indexPermissionCamera = permissionsRequestIndex;
      permissionsRequestIndex++;
    }

    if (requestIrisPermission) {
      permissionsToRequest[permissionsRequestIndex] = "com.id2mp.permissions.IRIS";
      indexPermissionIris = permissionsRequestIndex;
      permissionsRequestIndex++;
    }

    if (requestStoragePermission) {
      permissionsToRequest[permissionsRequestIndex] =
          Manifest.permission.WRITE_EXTERNAL_STORAGE;
      indexPermissionWrite = permissionsRequestIndex;
      permissionsRequestIndex++;
      permissionsToRequest[permissionsRequestIndex] =
          Manifest.permission.READ_EXTERNAL_STORAGE;
      indexPermissionRead = permissionsRequestIndex;
      permissionsRequestIndex++;
    }
    requestPermissions(permissionsToRequest, REQUEST_PERMISSION_CODE);
  }

  @Override
  public void onRequestPermissionsResult(int requestCode,
                                         String permissions[], int[] grantResults) {

    if (requestCameraPermission) {
      if ((grantResults.length >= indexPermissionCamera + 1) &&
          (grantResults[indexPermissionCamera] ==
              PackageManager.PERMISSION_GRANTED)) {
        hasCameraPermission = true;
      } else {
        permissionDenied = true;
      }
    }
    if (requestIrisPermission) {
      if ((grantResults.length >= indexPermissionIris + 1) &&
          (grantResults[indexPermissionIris] ==
              PackageManager.PERMISSION_GRANTED)) {
        hasIrisPermission = true;
      } else {
        permissionDenied = true;
      }
    }

    if (requestStoragePermission) {
      if ((grantResults.length >= indexPermissionRead + 1) &&
          (grantResults[indexPermissionWrite] ==
              PackageManager.PERMISSION_GRANTED) &&
          (grantResults[indexPermissionRead] ==
              PackageManager.PERMISSION_GRANTED)) {
        hasStoragePermission = true;
      } else {
        permissionDenied = true;
      }
    }

    if (hasCameraPermission && hasStoragePermission) {
      handleSuccess();
    } else if (permissionDenied) {
      handleFailure();
    }
  }

  private void handleSuccess() {
    startNewActivity();
    finish();
  }

  private void handleFailure() {
    if (!hasCameraPermission) {
      Toast.makeText(this, getAppName() + " requires access to camera. Go to Settings->Apps->" + "getAppName()" + "->Permissions set \"Camera\" switch to on.", Toast.LENGTH_LONG).show();
    }
    if (!hasStoragePermission) {
      Toast.makeText(this, getAppName() + " requires to write to external storage. Go to Settings->Apps->"+ getAppName() + "->Storage. Set \"Storage\" switch to on", Toast.LENGTH_LONG).show();
    }
  }

  abstract protected void startNewActivity();
  abstract protected String getAppName();

}
