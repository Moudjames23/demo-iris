package com.deltaid.sample_utils;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.ToneGenerator;

public class AudioBeep {
  private static String deviceId;
  private static ToneGenerator tg;
  private static AudioTrack track1, track2;
  private static int trackPitch;

  private static void setDeviceId() {
    if (deviceId == null || deviceId.length() == 0) {
      deviceId = "_" + android.os.Build.PRODUCT;
      deviceId.replace("-", "_");
    }
  }

  public static void beep(int numBeeps) {
    beep(numBeeps, 880);
  }

  public static void beep(int numBeeps, int pitch) {
    // beep can be made dependant on device if needed
    // but for now use AudioTrack beep by default for all devices
    //setDeviceId();
    ATBeep(numBeeps, pitch);
  }

  // Beep based on ToneGenerator (TG)
  private static void TGBeep(int numBeeps) {
    if (tg == null) {
      try {
        tg = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, ToneGenerator.MAX_VOLUME);
      } catch (Exception e) {
      }
    }
    tg.startTone(numBeeps > 1 ? ToneGenerator.TONE_PROP_BEEP2 : ToneGenerator.TONE_PROP_BEEP);
  }

  // Beep based on AudioTrack (AT)
  private static synchronized void ATBeep(int numBeeps, int pitch) {
    if (track1 == null || track2 == null || trackPitch != pitch) {
      trackPitch = pitch;
      int sampleRate = AudioTrack.getNativeOutputSampleRate(AudioManager.STREAM_NOTIFICATION);
      sampleRate = Math.min(sampleRate, 44100);
      short [] v = new short[(sampleRate / 20) & ~0xff];
      if (track1 != null) {
        track1.release(); // audio tracks are a very limited resource, we cannot wait for garbage collection
      }
      if (track2 != null) {
        track2.release(); // audio tracks are a very limited resource, we cannot wait for garbage collection
      }
      track1 = new AudioTrack(AudioManager.STREAM_NOTIFICATION, sampleRate, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT, v.length, AudioTrack.MODE_STATIC);
      track2 = new AudioTrack(AudioManager.STREAM_NOTIFICATION, sampleRate, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT, 2 * v.length, AudioTrack.MODE_STATIC);
      int sum = 0;
      for (int i = 0; i < v.length; i++) {
        v[i] = (short)(16000 * Math.sin(i * 2 * Math.PI / (v.length - 1)) * Math.sin(i * 2 * Math.PI * pitch / sampleRate));
        sum += v[i];
      }
      track1.write(v, 0, v.length);
      track2.write(v, 0, v.length);
      track2.write(v, 0, v.length);
    }
    AudioTrack track = numBeeps > 1 ? track2 : track1;
    track.stop();
    track.reloadStaticData();
    track.play();
  }
}
