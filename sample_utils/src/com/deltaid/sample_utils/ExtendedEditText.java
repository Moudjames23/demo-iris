package com.deltaid.sample_utils;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

/*
 * This class detects when EditText is done. To do so it has a onFocusChange and EditorAction listeners.
 * Moreover it overrides the onKeyPreIme to detect keyboard dismissal.
 */

public class ExtendedEditText extends EditText implements TextView.OnEditorActionListener, View.OnFocusChangeListener  {
  private OnEditingDoneListener listener;
  public ExtendedEditText(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    addListeners();
  }

  public ExtendedEditText(Context context, AttributeSet attrs) {
    super(context, attrs);
    addListeners();
  }

  public ExtendedEditText(Context context) {
    super(context);
    addListeners();
  }

  private void addListeners() {
    setOnEditorActionListener(this);
    setOnFocusChangeListener(this);
  }

  public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
    if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_PREVIOUS) {
      handleEditDone();
    }
    return false;
  }

  public void onFocusChange(View v, boolean hasFocus) {
    if (!hasFocus) {
      handleEditDone();
    }
  }

  private void handleEditDone() {
    if (listener != null) {
      listener.onEditingDone(this);
    }
  }

  @Override
  public boolean onKeyPreIme(int keyCode, KeyEvent event) {
    if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
      handleEditDone();
    }
    return super.onKeyPreIme(keyCode, event);
  }

  public void setOnEditingDoneListener(OnEditingDoneListener l) {
    listener = l;
  }

  public interface OnEditingDoneListener {
    public void onEditingDone(ExtendedEditText eet);
  }
}
