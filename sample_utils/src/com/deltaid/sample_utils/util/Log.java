package com.deltaid.sample_utils.util;

public class Log {
  public static boolean LOG_DEBUG = false;

  public static int d(String tag, String msg) {
    if (LOG_DEBUG) {
      return android.util.Log.d(tag, msg);
    }
    return 0;
  }

  public static int d(String tag, String msg, Throwable tr) {
  if (LOG_DEBUG) {
    return android.util.Log.d(tag, msg, tr);
    }
  return 0;
  }

  public static int e(String tag, String msg) {
  if (LOG_DEBUG) {
    return android.util.Log.e(tag, msg);
  }
  return 0;
  }

  public static int e(String tag, String msg, Throwable tr) {
  if (LOG_DEBUG) {
    return android.util.Log.e(tag, msg, tr);
    }
  return 0;
  }

  public static int i(String tag, String msg) {
  if (LOG_DEBUG) {
    return android.util.Log.i(tag, msg);
  }
  return 0;
  }

  public static int i(String tag, String msg, Throwable tr) {
  if (LOG_DEBUG) {
    return android.util.Log.i(tag, msg, tr);
  }
  return 0;
  }

  public static int v(String tag, String msg) {
  if (LOG_DEBUG) {
    return android.util.Log.v(tag, msg);
  }
  return 0;
  }

  public static int v(String tag, String msg, Throwable tr) {
  if (LOG_DEBUG) {
    return android.util.Log.v(tag, msg, tr);
  }
  return 0;
  }

  public static int w(String tag, Throwable tr) {
    if (LOG_DEBUG) {
      return (android.util.Log.w(tag, tr));
    }
    return 0;
  }

  public static int w(String tag, String msg, Throwable tr) {
  if (LOG_DEBUG) {
    return (android.util.Log.w(tag, msg, tr));
    }
    return 0;
  }

  public static int w(String tag, String msg) {
  if (LOG_DEBUG) {
    return (android.util.Log.w(tag, msg));
    }
    return 0;
  }

  public static int wtf(String tag, Throwable tr) {
  if (LOG_DEBUG) {
    return (android.util.Log.wtf(tag, tr));
    }
    return 0;
  }

  public static int wtf(String tag, String msg) {
  if (LOG_DEBUG) {
    return (android.util.Log.wtf(tag, msg));
    }
    return 0;
  }

  public static int wtf(String tag, String msg, Throwable tr) {
  if (LOG_DEBUG) {
      return (android.util.Log.wtf(tag, msg, tr));
  }
  return 0;
  }
}
