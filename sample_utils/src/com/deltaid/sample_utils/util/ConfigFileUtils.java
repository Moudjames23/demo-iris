package com.deltaid.sample_utils.util;

import android.util.Pair;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ConfigFileUtils {

  static public String readFileAsString(String path) {
    String ret = null;
    try {
      FileInputStream fin = new FileInputStream(path);
      ret = convertStreamToString(fin);
      fin.close();
    } catch (Exception ex) {
    }
    return  ret;
  }

  static private String convertStreamToString(InputStream is) throws Exception {
    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
    StringBuilder sb = new StringBuilder();
    String line = null;
    while ((line = reader.readLine()) != null) {
      sb.append(line);
    }
    reader.close();
    return sb.toString();
  }

  private static void saveJasonFile(String path, JSONObject json) {
    try (FileWriter file = new FileWriter(path)) {
      file.write(json.toString());
      file.flush();
    } catch (IOException ioe) {
      ioe.printStackTrace();
    }
  }

  static public void writeJsonConfigFile(String path, Pair<String, Object>[] props) {
    JSONObject cfg = new JSONObject();
    JSONObject map = new JSONObject();
    try {
      for (Pair<String, Object> p : props) {
        map.put(p.first, p.second);
      }
      cfg.put("config", map);
    } catch (JSONException je) {
      je.printStackTrace();
    }
    saveJasonFile(path, cfg);
  }

  static public boolean getSConfigFileBooleanProperty(String filePath, String property) {
    String cfg = readFileAsString(filePath);
    boolean retVal = false;
    if (cfg != null) {
      try {
        JSONObject cfgJson = new JSONObject(cfg);
        JSONObject config = cfgJson.getJSONObject("config");
        if (config.has(property)) {
          retVal = config.getBoolean(property);
        }
      } catch (Exception ex) {
      }
    }
    return retVal;
  }

  static public int getSConfigFileIntProperty(String filePath, String property) {
    String cfg = readFileAsString(filePath);
    int retVal = 0;
    if (cfg != null) {
      try {
        JSONObject cfgJson = new JSONObject(cfg);
        JSONObject config = cfgJson.getJSONObject("config");
        if (config.has(property)) {
          retVal = config.getInt(property);
        }
      } catch (Exception ex) {
      }
    }
    return retVal;
  }

  static public void setConfigFileBooleanProperty(String path, String property, boolean val) {
    String cfg = readFileAsString(path);
    if (cfg != null) {
      try {
        JSONObject cfgJson = new JSONObject(cfg);
        JSONObject config = cfgJson.getJSONObject("config");
        if (config != null) {
          config.put(property, val);
        }
        cfgJson.put("config", config);
        saveJasonFile(path, cfgJson);
      } catch (Exception ex) {
      }
    }
  }

  static public void setConfigFileIntProperty(String path, String property, int val) {
    String cfg = readFileAsString(path);
    if (cfg != null) {
      try {
        JSONObject cfgJson = new JSONObject(cfg);
        JSONObject config = cfgJson.getJSONObject("config");
        if (config != null) {
          config.put(property, val);
        }
        cfgJson.put("config", config);
        saveJasonFile(path, cfgJson);
      } catch (Exception ex) {
      }
    }
  }

  static {
    System.loadLibrary("sample_utils_jni");
  }
  public native static int convert_to_argb(byte[] pixels_in, int stride_in, int xoffset_in, int yoffset_in, int[] pixels_out, int width_out, int height_out, boolean isRaw10);
  // convert to argb and map a range of pixel values to a specified color
  public native static int convert_to_argb_map_range(byte[] pixels_in, int stride_in, int xoffset_in, int yoffset_in, int[] pixels_out, int width_out, int height_out, boolean isRaw10,
                                                     int range_min_pixel, int range_max_pixel,
                                                     int red_val, int green_val, int blue_val);
  public native static int raw10_to_8bpp_in_place(byte [] pixles_in, int stride_in, int width_in, int height_in);
  //public native static int downsample2x2_raw10_in_place(byte [] pixles_in, int stride_in, int width_in);
  //public native static int img_resize_bilinear(byte [] pixels_in, int stride_in, int width_in, int height_in, byte [] pixels_out, int width_out, int height_out, boolean isRaw10);
  //public native static int set_cpu_affinity(long affinity);
}
