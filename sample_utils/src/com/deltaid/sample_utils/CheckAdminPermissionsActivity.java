package com.deltaid.sample_utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.Toast;

abstract public class CheckAdminPermissionsActivity extends Activity {
  private static final int REQUEST_CODE_ASK_DISABLE_KEYGUARD_PERMISSION = 0x01;
  private static final int REQUEST_CODE_ASK_RECEIVE_BOOT_COMPLETED_PERMISSION = 0x02;
  private static final int REQUEST_CODE_ASK_READ_PHONE_STATE_PERMISSION = 0x03;

  private static int REQUEST_PERMISSION_CODE = 1;
  private int indexPermissionKeyguard;
  private int indexPermissionReceiveBootCompleted;
  private int indexPermissionReadPhoneState;
  private boolean requestKeyguardPermission;
  private boolean requestBootCompletedPermission;
  private boolean requestPhoneStatePermission;
  private int numPermissionsToRequest;
  private boolean hasKeyguardPermission;
  private boolean hasBootCompletedPermission;
  private boolean hasPhoneStatePermission;
  private boolean permissionDenied;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public void onResume() {
    super.onResume();
    if (!permissionDenied) {
      numPermissionsToRequest = 0;
      checkPermissions();
    } else {
      permissionDenied = false;
    }
  }

  private void checkPermissions() {
    checkKeyguardPermission();
    checkBootCompletedPermission();
    checkPhoneStatePermission();
    if (numPermissionsToRequest != 0) {
      buildPermissionsRequest();
    } else {
      handleSuccess();
    }
  }

  @TargetApi(23)
  private void checkKeyguardPermission() {
    if (checkSelfPermission(Manifest.permission.DISABLE_KEYGUARD)
        != PackageManager.PERMISSION_GRANTED) {
      numPermissionsToRequest++;
      requestKeyguardPermission = true;
    } else {
      hasKeyguardPermission = true;
    }
  }

  @TargetApi(23)
  private void checkBootCompletedPermission() {
    if (checkSelfPermission(Manifest.permission.RECEIVE_BOOT_COMPLETED)
        != PackageManager.PERMISSION_GRANTED) {
      numPermissionsToRequest++;
      requestBootCompletedPermission = true;
    } else {
      hasBootCompletedPermission = true;
    }
  }

  @TargetApi(23)
  private void checkPhoneStatePermission() {
    if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE)
        != PackageManager.PERMISSION_GRANTED) {
      numPermissionsToRequest++;
      requestPhoneStatePermission = true;
    } else {
      hasPhoneStatePermission = true;
    }
  }

  @TargetApi(23)
  private void buildPermissionsRequest() {
    String[] permissionsToRequest = new String[numPermissionsToRequest];
    int permissionsRequestIndex = 0;

    if (requestKeyguardPermission) {
      permissionsToRequest[permissionsRequestIndex] = Manifest.permission.DISABLE_KEYGUARD;
      indexPermissionKeyguard = permissionsRequestIndex;
      permissionsRequestIndex++;
    }

    if (requestBootCompletedPermission) {
      permissionsToRequest[permissionsRequestIndex] =
          Manifest.permission.RECEIVE_BOOT_COMPLETED;
      indexPermissionReceiveBootCompleted = permissionsRequestIndex;
      permissionsRequestIndex++;
    }

    if (requestPhoneStatePermission) {
      permissionsToRequest[permissionsRequestIndex] =
          Manifest.permission.READ_PHONE_STATE;
      indexPermissionReadPhoneState = permissionsRequestIndex;
      permissionsRequestIndex++;
    }
    requestPermissions(permissionsToRequest, REQUEST_PERMISSION_CODE);
  }

  @Override
  public void onRequestPermissionsResult(int requestCode,
                                         String permissions[], int[] grantResults) {

    if (requestKeyguardPermission) {
      if ((grantResults.length >= indexPermissionKeyguard + 1) &&
          (grantResults[indexPermissionKeyguard] ==
              PackageManager.PERMISSION_GRANTED)) {
        hasKeyguardPermission = true;
      } else {
        permissionDenied = true;
      }
    }
    if (requestBootCompletedPermission) {
      if ((grantResults.length >= indexPermissionReceiveBootCompleted + 1) &&
          (grantResults[indexPermissionReceiveBootCompleted] ==
              PackageManager.PERMISSION_GRANTED)) {
        hasBootCompletedPermission = true;
      } else {
        permissionDenied = true;
      }
    }
    if (requestPhoneStatePermission) {
      if ((grantResults.length >= indexPermissionReadPhoneState + 1) &&
          (grantResults[indexPermissionReadPhoneState] ==
              PackageManager.PERMISSION_GRANTED)) {
        hasPhoneStatePermission = true;
      } else {
        permissionDenied = true;
      }
    }

    if (hasKeyguardPermission && hasBootCompletedPermission && hasPhoneStatePermission) {
      handleSuccess();
    } else if (permissionDenied) {
      handleFailure();
    }
  }

  private void handleSuccess() {
    startNewActivity();
    finish();
  }
  private void handleFailure() {
    if (!hasKeyguardPermission || !hasBootCompletedPermission || !hasPhoneStatePermission) {
      Toast.makeText(this, getAppName() + " requires permission to make and manage phone calls. Go to Settings->Apps->" + "getAppName()" + "->Permissions set \"Phone\" switch to on.", Toast.LENGTH_LONG).show();
    }
  }

  abstract protected void startNewActivity();
  abstract protected String getAppName();
}
