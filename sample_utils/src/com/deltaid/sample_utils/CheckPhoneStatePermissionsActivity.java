package com.deltaid.sample_utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.Toast;

abstract public class CheckPhoneStatePermissionsActivity extends Activity {
  private static int REQUEST_PERMISSION_CODE = 1;
  private int indexPermissionReadPhoneState;
  private boolean requestPhoneStatePermission;
  private int numPermissionsToRequest;
  private boolean hasPhoneStatePermission;
  private boolean permissionDenied;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public void onResume() {
    super.onResume();
    if (!permissionDenied) {
      numPermissionsToRequest = 0;
      checkPermissions();
    } else {
      permissionDenied = false;
    }
  }

  private void checkPermissions() {
    checkPhoneStatePermission();
    if (numPermissionsToRequest != 0) {
      buildPermissionsRequest();
    } else {
      handleSuccess();
    }
  }


  @TargetApi(23)
  private void checkPhoneStatePermission() {
    if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE)
        != PackageManager.PERMISSION_GRANTED) {
      numPermissionsToRequest++;
      requestPhoneStatePermission = true;
    } else {
      hasPhoneStatePermission = true;
    }
  }

  @TargetApi(23)
  private void buildPermissionsRequest() {
    String[] permissionsToRequest = new String[numPermissionsToRequest];
    int permissionsRequestIndex = 0;

    if (requestPhoneStatePermission) {
      permissionsToRequest[permissionsRequestIndex] =
          Manifest.permission.READ_PHONE_STATE;
      indexPermissionReadPhoneState = permissionsRequestIndex;
      permissionsRequestIndex++;
    }
    requestPermissions(permissionsToRequest, REQUEST_PERMISSION_CODE);
  }

  @Override
  public void onRequestPermissionsResult(int requestCode,
                                         String permissions[], int[] grantResults) {
    if (requestPhoneStatePermission) {
      if ((grantResults.length >= indexPermissionReadPhoneState + 1) &&
          (grantResults[indexPermissionReadPhoneState] ==
              PackageManager.PERMISSION_GRANTED)) {
        hasPhoneStatePermission = true;
      } else {
        permissionDenied = true;
      }
    }

    if (hasPhoneStatePermission) {
      handleSuccess();
    } else if (permissionDenied) {
      handleFailure();
    }
  }

  private void handleSuccess() {
    startNewActivity();
    finish();
  }
  private void handleFailure() {
    if (!hasPhoneStatePermission) {
      Toast.makeText(this, getAppName() + " requires permission to make and manage phone calls. Go to Settings->Apps->" + "getAppName()" + "->Permissions set \"Phone\" switch to on.", Toast.LENGTH_LONG).show();
    }
  }

  abstract protected void startNewActivity();
  abstract protected String getAppName();
}
