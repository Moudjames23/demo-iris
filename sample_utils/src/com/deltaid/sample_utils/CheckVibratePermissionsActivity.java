package com.deltaid.sample_utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.Toast;

abstract public class CheckVibratePermissionsActivity extends Activity {
  private static int REQUEST_PERMISSION_CODE = 1;
  private int indexPermissionVibrate;
  private boolean requestVibratePermission;
  private int numPermissionsToRequest;
  private boolean hasVibratePermission;
  private boolean permissionDenied;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public void onResume() {
    super.onResume();
    if (!permissionDenied) {
      numPermissionsToRequest = 0;
      checkPermissions();
    } else {
      permissionDenied = false;
    }
  }

  private void checkPermissions() {
    checkVibratePermission();
    if (numPermissionsToRequest != 0) {
      buildPermissionsRequest();
    } else {
      handleSuccess();
    }
  }


  @TargetApi(23)
  private void checkVibratePermission() {
    if (checkSelfPermission(Manifest.permission.VIBRATE)
        != PackageManager.PERMISSION_GRANTED) {
      numPermissionsToRequest++;
      requestVibratePermission = true;
    } else {
      hasVibratePermission = true;
    }
  }

  @TargetApi(23)
  private void buildPermissionsRequest() {
    String[] permissionsToRequest = new String[numPermissionsToRequest];
    int permissionsRequestIndex = 0;

    if (requestVibratePermission) {
      permissionsToRequest[permissionsRequestIndex] =
          Manifest.permission.VIBRATE;
      indexPermissionVibrate = permissionsRequestIndex;
      permissionsRequestIndex++;
    }
    requestPermissions(permissionsToRequest, REQUEST_PERMISSION_CODE);
  }

  @Override
  public void onRequestPermissionsResult(int requestCode,
                                         String permissions[], int[] grantResults) {
    if (requestVibratePermission) {
      if ((grantResults.length >= indexPermissionVibrate + 1) &&
          (grantResults[indexPermissionVibrate] ==
              PackageManager.PERMISSION_GRANTED)) {
        hasVibratePermission = true;
      } else {
        permissionDenied = true;
      }
    }

    if (hasVibratePermission) {
      handleSuccess();
    } else if (permissionDenied) {
      handleFailure();
    }
  }

  private void handleSuccess() {
    startNewActivity();
    finish();
  }
  private void handleFailure() {
    if (!hasVibratePermission) {
      Toast.makeText(this, getAppName() + " requires permission to make and manage phone calls. Go to Settings->Apps->" + "getAppName()" + "->Permissions set \"Vibrate\" switch to on.", Toast.LENGTH_LONG).show();
    }
  }

  abstract protected void startNewActivity();
  abstract protected String getAppName();
}
