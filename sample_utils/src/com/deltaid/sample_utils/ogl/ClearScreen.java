package com.deltaid.sample_utils.ogl;

import javax.microedition.khronos.opengles.GL10;

public class ClearScreen extends Mesh {
  private short[] indices = {0, 1, 2, 1, 3, 2};
  private float r, g, b;

  public ClearScreen(float cameraWidth, float cameraHeight, float width, float height, float ar, float rotation, float r, float g, float b) { // width, height in camera coord space
    setCameraSize(cameraWidth, cameraHeight);
    setScreenColor(r, g, b);
    this.ar = ar;
    arFactor = (cameraWidth / cameraHeight) / ar;
    width *= arFactor;
    setRotation(rotation);
    float w = width/camWidth, h = height/camHeight;
    float[] screenVertices = {
        -w, -h, 0,
         w, -h, 0,
        -w,  h, 0,
         w,  h, 0
    };

    setIndices(indices);
    setVertices(screenVertices);
  }

  public void setScreenColor(float r, float g, float b) {
    this.r = r;
    this.g = g;
    this.b = b;
  }

  public void draw(GL10 gl) {
    gl.glPushMatrix();
    // Counter-clockwise winding.
    gl.glFrontFace(GL10.GL_CCW);
    // Enable face culling.
    gl.glEnable(GL10.GL_CULL_FACE);
    // What faces to remove with the face culling.
    gl.glCullFace(GL10.GL_BACK);
    // Enabled the vertices buffer for writing and to be used during
    // rendering.
    gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
    // Specifies the location and data format of an array of vertex
    // coordinates to use when rendering.
    gl.glVertexPointer(3, GL10.GL_FLOAT, 0, mVerticesBuffer);
    // Set flat color
    //gl.glColor4f(mRGBA[0], mRGBA[1], mRGBA[2], mRGBA[3]);
    gl.glClearColor(r, g, b, 1f);
    gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
    gl.glTranslatef(xLoc, yLoc, zLoc);
    rotateContext(gl, rotation);
    // Disable the vertices buffer
    gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);

    // Disable face culling.
    gl.glDisable(GL10.GL_CULL_FACE);
    gl.glPopMatrix();
  }
}
