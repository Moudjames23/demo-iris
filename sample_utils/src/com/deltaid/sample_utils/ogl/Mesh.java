/**
 * Copyright 2010 Per-Erik Bergman (per-erik.bergman@jayway.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.deltaid.sample_utils.ogl;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.Vector;

import javax.microedition.khronos.opengles.GL10;

/**
 * Mesh is a base class for 3D objects making it easier to create and maintain
 * new primitives.
 *
 *
 */
public class Mesh {
  // vertex buffer.
  protected FloatBuffer mVerticesBuffer = null;
  // index buffer.
  protected ShortBuffer mIndicesBuffer = null;
  // UV texture buffer.
  protected FloatBuffer mTextureBuffer;
  // The number of indices.
  protected int mNumOfIndices = -1;

  protected float scale = 1;
  protected float rotation = 0;
  protected float dx = 0, dy = 0; // x,y translation

  private final float DEG2RAD = (float) (Math.PI / 180.0);
  protected float camWidth, camHeight;
  protected float ar = 1.0f; // aspect ratio
  protected float arFactor = 1;
  protected float xLoc = 0, yLoc = 0, zLoc = 0;
  protected int[] textures;
  private final Vector<Mesh> mChildren = new Vector<Mesh>();

  protected Mesh() {}

  /**
   * Render the mesh.
   *
   * @param gl
   *            the OpenGL context to render to.
   */
  public void draw(GL10 gl) {
    int size = mChildren.size();
    for (int i = 0; i < size; i++) {
      mChildren.get(i).draw(gl);
    }
  }

  /**
   * @param location
   * @param object
   * @see Vector#add(int, Object)
   */
  public void add(int location, Mesh object) {
    mChildren.add(location, object);
  }

  /**
   * @param object
   * @return
   * @see Vector#add(Object)
   */
  public boolean add(Mesh object) {
    if (!mChildren.contains(object)) {
      return mChildren.add(object);
    }
    return true;
  }

  /**
   *
   * @see Vector#clear()
   */
  public void clear() {
    mChildren.clear();
  }

  /**
   * @param location
   * @return
   * @see Vector#get(int)
   */
  public Mesh get(int location) {
    return mChildren.get(location);
  }

  /**
   * @param location
   * @return
   * @see Vector#remove(int)
   */
  public Mesh remove(int location) {
    return mChildren.remove(location);
  }

  /**
   * @param object
   * @return
   * @see Vector#remove(Object)
   */
  public boolean remove(Object object) {
    return mChildren.remove(object);
  }

  /**
   * @return
   * @see Vector#size()
   */
  public int size() {
    return mChildren.size();
  }


  public void setRotation(float r) {
    rotation = r % 360 + (r < 0 ? 360 : 0);
  }

  public void setOffset(float x, float y, float z, boolean isYMirrored) {
    int mult = isYMirrored ? -1 : 1;
    float cw2 = camWidth / 2, ch2 = camHeight / 2;
    xLoc = -(x / cw2 - 1) * arFactor;
    yLoc = -(y / ch2 - 1) * mult;
    zLoc = z;
  }

  protected void setCameraSize(float width, float height) {
    camWidth = width;
    camHeight = height;
  }

  protected void setTexture(int[] textures) {
    this.textures = textures;
  }

  public void setTextures(int[] txtr) {
    int size = mChildren.size();
    for (int i = 0; i < size; i++) {
      mChildren.get(i).setTexture(txtr);
    }
  }

  /**
   * Set the vertices.
   *
   * @param vertices
   */
  protected void setVertices(float[] vertices) {
    // a float is 4 bytes, therefore we multiply the number of vertices by 4.
    if (mVerticesBuffer != null) {
      mVerticesBuffer.clear();
    }
    ByteBuffer vbb = ByteBuffer.allocateDirect(vertices.length * 4);
    vbb.order(ByteOrder.nativeOrder());
    mVerticesBuffer = vbb.asFloatBuffer();
    mVerticesBuffer.put(vertices);
    mVerticesBuffer.position(0);
  }

  protected FloatBuffer setShapeVertices(float[] vertices) {
    ByteBuffer vbb = ByteBuffer.allocateDirect(vertices.length * 4);
    vbb.order(ByteOrder.nativeOrder());
    FloatBuffer vbuffer = vbb.asFloatBuffer();
    vbuffer.put(vertices);
    vbuffer.position(0);
    return vbuffer;
  }

  protected void drawCircle(GL10 gl, float xc, float yc, float radius) {
    float[] circle = new float[1080];
    for (int i = 0; i < 360; i++) {
      double deginrad = i * DEG2RAD;
      int idx = 3 * i;
      circle[idx] = (float) (radius * Math.cos(deginrad) + xc);
      circle[idx + 1] = (float) (radius * Math.sin(deginrad) + yc);
      circle[idx + 2] = 0.0f;
    }
    FloatBuffer vbuff = setShapeVertices(circle);
    gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vbuff);
    gl.glDrawArrays(GL10.GL_LINE_LOOP, 0, 360);
  }

  protected void drawRect(GL10 gl, float x1, float y1, float x2, float y2) {
    float[] dirVerts = new float[4 * 3 * 2];
    dirVerts[0] = x1;
    dirVerts[1] = y1;
    dirVerts[2] = 0.0f;
    dirVerts[3] = x2;
    dirVerts[4] = y1;
    dirVerts[5] = 0.0f;

    dirVerts[6] = x2;
    dirVerts[7] = y1;
    dirVerts[8] = 0.0f;
    dirVerts[9] = x2;
    dirVerts[10] = y2;
    dirVerts[11] = 0.0f;

    dirVerts[12] = x1;
    dirVerts[13] = y2;
    dirVerts[14] = 0.0f;
    dirVerts[15] = x2;
    dirVerts[16] = y2;
    dirVerts[17] = 0.0f;

    dirVerts[18] = x1;
    dirVerts[19] = y1;
    dirVerts[20] = 0.0f;
    dirVerts[21] = x1;
    dirVerts[22] = y2;
    dirVerts[23] = 0.0f;

    FloatBuffer dbuff = setShapeVertices(dirVerts);
    gl.glVertexPointer(3, GL10.GL_FLOAT, 0, dbuff);
    gl.glDrawArrays(GL10.GL_LINES, 0, 8);
  }

  /**
   * Set the indices.
   *
   * @param indices
   */
  protected void setIndices(short[] indices) {
    // short is 2 bytes, therefore we multiply the number if
    // vertices with 2.
    ByteBuffer ibb = ByteBuffer.allocateDirect(indices.length * 2);
    ibb.order(ByteOrder.nativeOrder());
    mIndicesBuffer = ibb.asShortBuffer();
    mIndicesBuffer.put(indices);
    mIndicesBuffer.position(0);
    mNumOfIndices = indices.length;
  }

  /**
   * Set the texture coordinates.
   *
   * @param textureCoords
   */
  protected void setTextureCoordinates(float[] textureCoords) {
    // float is 4 bytes, therefore we multiply the number if
    // vertices with 4.
    ByteBuffer byteBuf = ByteBuffer.allocateDirect(textureCoords.length * 4);
    byteBuf.order(ByteOrder.nativeOrder());
    mTextureBuffer = byteBuf.asFloatBuffer();
    mTextureBuffer.put(textureCoords);
    mTextureBuffer.position(0);
  }

  protected void rotateContext(GL10 gl, float rotation) {
    int rot = (int)rotation / 90 * 90;

    switch (rot) {
      case 90:
        gl.glRotatef(90, 0, 0, 1);
        break;
      case 270:
        gl.glRotatef(270, 0, 0, 1);
        break;
      case 180:
        gl.glRotatef(0, 0, 0, 1);
        break;
      case 0:
        gl.glRotatef(180, 0, 0, 1);
        break;
    }
  }

  // scale to maintain aspect ratio
  protected void scale2KeepAR(GL10 gl, float rotation) {
    int rot = (int)rotation / 90 * 90;
    float arx = ar;
    float ary = 1.0f;

    if (rot == 90 || rot == 270 || rot == 0) {
      float tmp;
      tmp = arx;
      arx = ary;
      ary = tmp;
    }
    gl.glScalef(arx, ary, 1);
  }
}
