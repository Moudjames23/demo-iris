package com.deltaid.sample_utils.ogl;

import javax.microedition.khronos.opengles.GL10;

public class RectangleBorder extends Mesh {
  private float rectX1, rectY1, rectX2, rectY2;

  public RectangleBorder(float cameraWidth, float cameraHeight, float ar, float rotation) {
    setCameraSize(cameraWidth, cameraHeight);
    setRotation(rotation);
    this.ar = ar;
  }

  @Override
  public void draw(GL10 gl) {
    gl.glPushMatrix();
    // Enable face culling.
    gl.glEnable(GL10.GL_CULL_FACE);
    // Enabled the vertices buffer for writing and to be used during
    // rendering.
    gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
    gl.glTranslatef(dx, dy, 0);
    rotateContext(gl, rotation);
    scale2KeepAR(gl, rotation);
    gl.glColor4f(1, 0, 0, 1);

    gl.glEnable(GL10.GL_LINES);
    drawRect(gl, rectX1, rectY1, rectX2, rectY2);
    gl.glLineWidth(8.0f);
    gl.glDisable(GL10.GL_LINES);

    // Disable the vertices buffer
    gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);

    // Disable face culling.
    gl.glDisable(GL10.GL_CULL_FACE);
    gl.glPopMatrix();
  }

  public void setRect(float x1, float y1, float x2, float y2, boolean isYMirrored) {
    int mult = isYMirrored ? -1 : 1;
    float cw2 = camWidth / 2, ch2 = camHeight / 2;
    rectX1 = (x1 / cw2 - 1) * mult;
    rectY1 = (y1 / ch2 - 1) / ar;
    rectX2 = (x2 / cw2 - 1) * mult;
    rectY2 = (y2 / ch2 - 1) / ar;
  }

}
