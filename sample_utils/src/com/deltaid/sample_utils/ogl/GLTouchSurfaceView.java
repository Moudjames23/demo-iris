package com.deltaid.sample_utils.ogl;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

public class GLTouchSurfaceView extends GLSurfaceView {
  public Matrix matrix;

  static final int NONE = 0;
  static final int DRAG = 1;
  static final int ZOOM = 2;
  int mode = NONE;

  PointF last = new PointF();
  PointF start = new PointF();
  float minScale = 1f;
  float maxScale = 10f;
  float[] m;

  static final int CLICK = 3;
  float saveScale = 1f;

  private ScaleGestureDetector scaleDetector;
  private GestureDetector gestureDetector;
  private SimpleGestureCallbacks sgc;
  private boolean allowZoomScroll = true;

  Context context;

  public interface SimpleGestureCallbacks {
    public boolean onSingleTapConfirmed(MotionEvent event);
    public void onLongPress(MotionEvent event);
  }

  public GLTouchSurfaceView(Context context) {
    super(context);
    this.context = context;
    sharedConstructing(context);
  }

  public GLTouchSurfaceView(Context context, AttributeSet attrs) {
    super(context, attrs);
    sharedConstructing(context);
  }

  public void setSimpleGestureCallbacks(SimpleGestureCallbacks callback) {
    sgc = callback;
  }

  public void allowZoomScroll(boolean allow) {
    allowZoomScroll = allow;
  }

  private void sharedConstructing(Context context) {
    super.setClickable(true);
    this.context = context;
    scaleDetector = new ScaleGestureDetector(context, new GLTouchSurfaceView.ScaleListener());
    matrix = new Matrix();
    m = new float[9];
    gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener(){
      @Override
      public boolean onSingleTapConfirmed(MotionEvent event) {
        if (sgc != null) {
          return sgc.onSingleTapConfirmed(event);
        }
        return false;
      }

      @Override
      public void onLongPress(MotionEvent event) {
        if (sgc != null) {
          sgc.onLongPress(event);
        }
      }
    });

    setOnTouchListener(new OnTouchListener() {

      public boolean onTouch(View v, MotionEvent event) {
        if (allowZoomScroll) {
          scaleDetector.onTouchEvent(event);
          if (scaleDetector.isInProgress()) {
            return true;
          }
          gestureDetector.onTouchEvent(event);
          float xf = event.getX();
          float yf = event.getY();
          PointF curr = new PointF(-xf, yf);

          switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
              last.set(curr);
              start.set(last);
              mode = DRAG;
              break;

            case MotionEvent.ACTION_MOVE:
              if (mode == DRAG) {
                float deltaX = curr.x - last.x;
                float deltaY = curr.y - last.y;
                matrix.postTranslate(-deltaX, deltaY);
                fixTrans();
                last.set(curr.x, curr.y);
              }
              break;

            case MotionEvent.ACTION_UP:
              mode = NONE;
              int xDiff = (int) Math.abs(curr.x - start.x);
              int yDiff = (int) Math.abs(curr.y - start.y);
              if (xDiff < CLICK && yDiff < CLICK)
                performClick();
              break;

            case MotionEvent.ACTION_POINTER_UP:
              mode = NONE;
              break;
          }

          invalidate();
        }
        return true; // indicate event was handled
      }

    });
  }

  private class ScaleListener extends
      ScaleGestureDetector.SimpleOnScaleGestureListener {
    @Override
    public boolean onScaleBegin(ScaleGestureDetector detector) {
      mode = ZOOM;
      return true;
    }

    @Override
    public boolean onScale(ScaleGestureDetector detector) {
      float mScaleFactor = detector.getScaleFactor();
      float origScale = saveScale;
      saveScale *= mScaleFactor;
      if (saveScale > maxScale) {
        saveScale = maxScale;
        mScaleFactor = maxScale / origScale;
      } else if (saveScale < minScale) {
        saveScale = minScale;
        mScaleFactor = minScale / origScale;
      }

      matrix.postScale(mScaleFactor, mScaleFactor);
      fixTrans();
      invalidate();

      return true;
    }
  }

  void fixTrans() {
    matrix.getValues(m);

    m[Matrix.MTRANS_X] = getTrans(m[Matrix.MTRANS_X], getWidth(),  saveScale);
    m[Matrix.MTRANS_Y] = getTrans(m[Matrix.MTRANS_Y], getHeight(), saveScale);

    matrix.setValues(m);
  }

  float getTrans(float trans, float size, float scale) {
    float minTrans, maxTrans;

    minTrans = (1 - scale) * size;
    maxTrans = -minTrans;
    return Math.max(minTrans, Math.min(maxTrans, trans));
  }

  public float getScaleFactor() {
    matrix.getValues(m);
    return m[Matrix.MSCALE_X];
  }

  public void resetZoomScroll(float scale) {
    float sf = scale;
    float origScale = saveScale;
    saveScale *= scale;
    if (saveScale > scale) {
      saveScale = scale;
      sf = scale / origScale;
    }
    matrix.postScale(sf, sf);
    fixTrans();

    // undo any translations. Bring view back to center
    matrix.getValues(m);
    float dx = m[Matrix.MTRANS_X];
    float dy = m[Matrix.MTRANS_Y];
    matrix.postTranslate(-dx, -dy);

    invalidate();
  }

  public float getXTranslate() {
    matrix.getValues(m);
    return m[Matrix.MTRANS_X];
  }

  public float getYTranslate() {
    matrix.getValues(m);
    return m[Matrix.MTRANS_Y];
  }
}
