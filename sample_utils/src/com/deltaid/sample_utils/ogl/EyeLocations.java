package com.deltaid.sample_utils.ogl;

import javax.microedition.khronos.opengles.GL10;

public class EyeLocations extends Mesh {
  private float reyeX = 0, reyeY = 0, leyeX = 0, leyeY = 0;
  private float rIrisRadius = 0, lIrisRadius = 0;
  private float rPupilRadius = 0, lPupilRadius = 0;
  private final float[] mIrisCircle = {0.8f, 0.0f, 0.0f, 1.0f};
  private final float[] mPupilCircle = {0.0f, 0.8f, 0.0f, 1.0f};
  private float arFactor = 1;

  public EyeLocations(float cameraWidth, float cameraHeight, float ar, float rotation) {
    setCameraSize(cameraWidth, cameraHeight);
    setRotation(rotation);
    this.ar = ar;
    arFactor = (cameraWidth / cameraHeight) / ar;
  }

  @Override
  public void draw(GL10 gl) {
    gl.glPushMatrix();
    // Enable face culling.
    gl.glEnable(GL10.GL_CULL_FACE);
    // Enabled the vertices buffer for writing and to be used during
    // rendering.
    gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
    gl.glTranslatef(xLoc, yLoc, zLoc);
    rotateContext(gl, rotation);
    scale2KeepAR(gl, rotation);

    gl.glEnable(GL10.GL_LINE_LOOP);
    gl.glLineWidth(8.0f);
    gl.glColor4f(mIrisCircle[0], mIrisCircle[1], mIrisCircle[2], mIrisCircle[3]);
    if (lIrisRadius > 0f) {
      drawCircle(gl, leyeX, leyeY, lIrisRadius);
    }
    if (rIrisRadius > 0f) {
      drawCircle(gl, reyeX, reyeY, rIrisRadius);
    }
    gl.glColor4f(mPupilCircle[0], mPupilCircle[1], mPupilCircle[2], mPupilCircle[3]);
    if (lPupilRadius > 0f) {
      drawCircle(gl, leyeX, leyeY, lPupilRadius);
    }
    if (rPupilRadius > 0f) {
      drawCircle(gl, reyeX, reyeY, rPupilRadius);
    }
    gl.glDisable(GL10.GL_LINE_LOOP);

    // Disable the vertices buffer
    gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);

    // Disable face culling.
    gl.glDisable(GL10.GL_CULL_FACE);
    gl.glPopMatrix();
  }

  public void setEyeParams(float xL, float yL, float xR, float yR, float rIrisL, float rIrisR, float rPupilL, float rPupilR, boolean isYMirrored) {
    int mult = isYMirrored ? -1 : 1;
    float cw2 = camWidth / 2, ch2 = camHeight / 2;

    leyeX = (xL / cw2 - 1) * arFactor * mult;
    leyeY = (yL / ch2 - 1) / ar;
    reyeX = (xR / cw2 - 1) * arFactor * mult;
    reyeY = (yR / ch2 - 1) / ar;

    lIrisRadius = rIrisL / cw2;
    rIrisRadius = rIrisR / cw2;

    lPupilRadius = rPupilL / cw2;
    rPupilRadius = rPupilR / cw2;
  }

  public void setIrisColor(float r, float g, float b, float a) {
    mIrisCircle[0] = r;
    mIrisCircle[1] = g;
    mIrisCircle[2] = b;
    mIrisCircle[3] = a;
  }
}
