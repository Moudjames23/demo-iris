package com.deltaid.sample_utils.ogl;

import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

public class PreviewCircles extends Mesh {
  private float xL = 0;
  private float yL = 0;
  private float xR = 0;
  private float yR = 0;
  private float rmin = 0;
  private float r = 0;
  private final int NUM_SPOKES = 80;
  private final int NUM_INNER_SPOKES = 20;
  private double spokeAngle = 2.0 * Math.PI / NUM_SPOKES;
  private final float DEG2RAD = (float) (Math.PI / 180.0);
  private float progress = 0;
  private boolean coloredPreviewCircle = false;
  private final float[] mRGBACircle = {1.0f, 1.0f, 1.0f, 1.0f};
  private final float[] mRGBACircleColored = {0.0f, 0.7f, 0.0f, 1.0f};
  private float diagR = 1.0f;

  public PreviewCircles(float cameraWidth, float cameraHeight, float ar, float rotation) {
    setCameraSize(cameraWidth, cameraHeight);
    setRotation(rotation);
    this.ar = ar;
  }

  @Override
  public void draw(GL10 gl) {
    gl.glPushMatrix();
    // Enable face culling.
    gl.glEnable(GL10.GL_CULL_FACE);
    // Enabled the vertices buffer for writing and to be used during
    // rendering.
    gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
    gl.glTranslatef(xLoc, yLoc, zLoc);

    rotateContext(gl, rotation);
    scale2KeepAR(gl, rotation);

    gl.glEnable(GL10.GL_LINE_LOOP);
    gl.glLineWidth(4.0f);
    if (coloredPreviewCircle) {
      gl.glColor4f(mRGBACircleColored[0], mRGBACircleColored[1], mRGBACircleColored[2], mRGBACircleColored[3]);
    } else {
      gl.glColor4f(mRGBACircle[0], mRGBACircle[1], mRGBACircle[2], mRGBACircle[3]);
    }

    drawCircle(gl, xL, yL, r);
    drawCircle(gl, xR, yR, r);

    drawCircle(gl, xL, yL, rmin);
    drawCircle(gl, xR, yR, rmin);

    gl.glDisable(GL10.GL_LINE_LOOP);

    gl.glLineWidth(2.0f);
    float rDelt = 50.0f / diagR;
    gl.glEnable(GL10.GL_LINES);

    drawInnerSpokes(gl, xL, yL, rmin, 20, rDelt);
    drawInnerSpokes(gl, xR, yR, rmin, 20, rDelt);

    drawDirLines(gl, xL, yL, r, rDelt);
    drawDirLines(gl, xR, yR, r, rDelt);

    gl.glLineWidth(4.0f);
    drawProgressSpokes(gl, r, rotation);
    gl.glDisable(GL10.GL_LINES);

    // Disable the vertices buffer
    gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);

    // Disable face culling.
    gl.glDisable(GL10.GL_CULL_FACE);
    gl.glPopMatrix();

  }

  private void drawInnerSpokes(GL10 gl, float xc, float yc, float radius, int angleDelta, float radiusDelta) {
    int numSpokes = 360 / angleDelta;
    float lineVerts[] = new float[numSpokes * 2 * 3];
    double angle = 0;
    float r2 = radius - radiusDelta;
    double deltaRad = angleDelta * DEG2RAD;
    for (int i = 0; i < numSpokes; i++) {
      int idx = 6 * i;
      double dcos = Math.cos(-angle);
      double dsin = Math.sin(-angle);
      lineVerts[idx] = (float) (xc + r2 * dcos);
      lineVerts[idx + 1] = (float) (yc + r2 * dsin);
      lineVerts[idx + 2] = 0.0f;
      lineVerts[idx + 3] = (float) (xc + radius * dcos);
      lineVerts[idx + 4] = (float) (yc + radius * dsin);
      lineVerts[idx + 5] = 0.0f;
      angle += deltaRad;
    }
    FloatBuffer lbuff = setShapeVertices(lineVerts);
    gl.glVertexPointer(3, GL10.GL_FLOAT, 0, lbuff);
    gl.glDrawArrays(GL10.GL_LINES, 0, 2 * numSpokes);
  }

  private void drawProgressSpokes(GL10 gl, float radius, float rotation) {
    float sweepAngle = progress * 3.6f;
    float rad1 = radius - 10.0f / diagR;
    float rad2 = radius - 50.0f / diagR;
    double angle = 0;
    double sangle = (sweepAngle * Math.PI) / 180.0;
    float[] pVerts = new float[360 * 2 * 3 * 2];
    int count = 0, idx = 0;
    double rotAngle = (rotation + 270.0) * Math.PI / 180.0;
    while (angle < sangle && ((idx = count * 12) < pVerts.length)) {
      double dcos = Math.cos(-angle + rotAngle);
      double dsin = Math.sin(-angle + rotAngle);
      pVerts[idx] = (float) (xL + rad2 * dcos);
      pVerts[idx + 1] = (float) (yL + rad2 * dsin);
      pVerts[idx + 2] = 0.0f;

      pVerts[idx + 3] = (float) (xL + rad1 * dcos);
      pVerts[idx + 4] = (float) (yL + rad1 * dsin);
      pVerts[idx + 5] = 0.0f;

      pVerts[idx + 6] = (float) (xR + rad2 * dcos);
      pVerts[idx + 7] = (float) (yR + rad2 * dsin);
      pVerts[idx + 8] = 0.0f;

      pVerts[idx + 9] = (float) (xR + rad1 * dcos);
      pVerts[idx + 10] = (float) (yR + rad1 * dsin);
      pVerts[idx + 11] = 0.0f;
      count++;
      angle += spokeAngle;
    }
    FloatBuffer pbuff = setShapeVertices(pVerts);
    gl.glVertexPointer(3, GL10.GL_FLOAT, 0, pbuff);
    gl.glDrawArrays(GL10.GL_LINES, 0, 4 * count);
  }

  private void drawDirLines(GL10 gl, float xc, float yc, float radius, float radiusDelta) {
    float[] dirVerts = new float[4 * 3 * 2];
    dirVerts[0] = xc;
    dirVerts[1] = yc - radius;
    dirVerts[2] = 0.0f;
    dirVerts[3] = xc;
    dirVerts[4] = yc - radius - radiusDelta;
    dirVerts[5] = 0.0f;

    dirVerts[6] = xc + radius;
    dirVerts[7] = yc;
    dirVerts[8] = 0.0f;
    dirVerts[9] = xc + radius + radiusDelta;
    dirVerts[10] = yc;
    dirVerts[11] = 0.0f;

    dirVerts[12] = xc;
    dirVerts[13] = yc + radius;
    dirVerts[14] = 0.0f;
    dirVerts[15] = xc;
    dirVerts[16] = yc + radius + radiusDelta;
    dirVerts[17] = 0.0f;

    dirVerts[18] = xc - radius;
    dirVerts[19] = yc;
    dirVerts[20] = 0.0f;
    dirVerts[21] = xc - radius - radiusDelta;
    dirVerts[22] = yc;
    dirVerts[23] = 0.0f;

    FloatBuffer dbuff = setShapeVertices(dirVerts);
    gl.glVertexPointer(3, GL10.GL_FLOAT, 0, dbuff);
    gl.glDrawArrays(GL10.GL_LINES, 0, 8);
  }

  public void setCircleParams(float xL, float yL, float xR, float yR, float rmin, float r, boolean isYMirrored) {
    int mult = isYMirrored ? -1 : 1;
    diagR = (float) Math.sqrt(camWidth * camWidth + camHeight * camHeight) / 2;
    float cw2 = camWidth / 2, ch2 = camHeight / 2;

    this.xL = (xL  / cw2 - 1) * mult;
    this.yL = (yL  / ch2 - 1);
    this.xR = (xR  / cw2 - 1) * mult;
    this.yR = (yR  / ch2 - 1);

    this.rmin = rmin / diagR;
    this.r = r / diagR;
  }

  public void setProgress(float prog) {
    progress = prog;
  }

  public void coloredPreviewCircle(boolean enabled) {
    coloredPreviewCircle = enabled;
  }

}
