package com.deltaid.sample_utils.ogl;

import android.graphics.Color;

import javax.microedition.khronos.opengles.GL10;

public class EyeSockets extends Mesh {
  private float reyeX = 0, reyeY = 0, leyeX = 0, leyeY = 0;
  private float rIrisRadius = 0, lIrisRadius = 0;

  private final int color = Color.YELLOW; // Color.argb(128, 204, 204, 0);
  private float arFactor = 1;

  public EyeSockets(float cameraWidth, float cameraHeight, float ar, float rotation) {
    setCameraSize(cameraWidth, cameraHeight);
    setRotation(rotation);
    this.ar = ar;
    arFactor = (cameraWidth / cameraHeight) / ar;
  }

  @Override
  public void draw(GL10 gl) {
    gl.glPushMatrix();
    // Enable face culling.
    gl.glEnable(GL10.GL_CULL_FACE);
    // Enabled the vertices buffer for writing and to be used during
    // rendering.
    gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
    gl.glTranslatef(xLoc, yLoc, zLoc);
    rotateContext(gl, rotation);
    scale2KeepAR(gl, rotation);

    gl.glEnable(GL10.GL_LINE_LOOP);
    gl.glLineWidth(1.0f);
    gl.glColor4f(Color.red(color) / 256.0f, Color.green(color) / 256.0f, Color.blue(color) / 256.0f, Color.alpha(color) / 256.0f);
    gl.glBlendFunc(GL10.GL_SRC_ALPHA,GL10.GL_ONE_MINUS_SRC_ALPHA);
    gl.glEnable(GL10.GL_BLEND);
    if (lIrisRadius > 0f) {
      float hs = lIrisRadius * 1.6f;
      drawRect(gl, leyeX - hs, leyeY - hs, leyeX + hs, leyeY + hs);
    }
    if (rIrisRadius > 0f) {
      float hs = rIrisRadius * 1.6f;
      drawRect(gl, reyeX - hs, reyeY - hs, reyeX + hs, reyeY + hs);
    }
    gl.glDisable(GL10.GL_LINE_LOOP);

    // Disable the vertices buffer
    gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);

    // Disable face culling.
    gl.glDisable(GL10.GL_CULL_FACE);
    gl.glPopMatrix();
  }

  public void setEyeParams(float xL, float yL, float xR, float yR, float rIrisL, float rIrisR, boolean isYMirrored) {
    int mult = isYMirrored ? -1 : 1;
    float cw2 = camWidth / 2, ch2 = camHeight / 2;

    leyeX = (xL / cw2 - 1) * arFactor * mult;
    leyeY = (yL / ch2 - 1) / ar;
    reyeX = (xR / cw2 - 1) * arFactor * mult;
    reyeY = (yR / ch2 - 1) / ar;

    lIrisRadius = rIrisL / cw2;
    rIrisRadius = rIrisR / cw2;
  }
}
