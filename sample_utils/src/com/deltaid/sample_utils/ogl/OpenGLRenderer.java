/**
 * Copyright 2010 Per-Erik Bergman (per-erik.bergman@jayway.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.deltaid.sample_utils.ogl;

import android.opengl.GLSurfaceView.Renderer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class OpenGLRenderer implements Renderer {
  private final Mesh root;
  private float scale = 1;
  private float dx = 0, dy = 0; // x,y translations
  //private int camWidth, camHeight;
  private int viewWidth, viewHeight;
  //private float xFactor = 1, yFactor = 1;
  private int[] textures = null;
  private int numTextures = 1;

  public OpenGLRenderer(/*int cameraWidth, int cameraHeight, */int numTextures) {
    // Initialize our root.
    root = new Mesh();
    //camWidth = cameraWidth;
    //camHeight = cameraHeight;
    this.numTextures = numTextures;
    textures = new int[numTextures];
  }

  /*
   * (non-Javadoc)
   *
   * @see
   * android.opengl.GLSurfaceView.Renderer#onSurfaceCreated(javax.microedition
   * .khronos.opengles.GL10, javax.microedition.khronos.egl.EGLConfig)
   */
  public void onSurfaceCreated(GL10 gl, EGLConfig config) {
    gl.glGenTextures(numTextures, textures, 0);
  }

  /*
   * (non-Javadoc)
   *
   * @see
   * android.opengl.GLSurfaceView.Renderer#onDrawFrame(javax.microedition.
   * khronos.opengles.GL10)
   */
  public void onDrawFrame(GL10 gl) {
    // Clears the screen and depth buffer.
    gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
    gl.glLoadIdentity();
    gl.glScalef(scale, scale, 1);
    gl.glTranslatef(dx / (scale * ( viewWidth / 2.0f)), dy / (scale * ( viewHeight / 2.0f )), 0);
    // Draw our scene.
    root.draw(gl);
  }

  /*
   * (non-Javadoc)
   *
   * @see
   * android.opengl.GLSurfaceView.Renderer#onSurfaceChanged(javax.microedition
   * .khronos.opengles.GL10, int, int)
   */
  public void onSurfaceChanged(GL10 gl, int width, int height) {
    viewWidth = width;
    viewHeight = height;
    //xFactor = camWidth / viewWidth;
    //yFactor = camHeight / viewHeight;
  }

  /**
   * Adds a mesh to the root.
   *
   * @param mesh
   *            the mesh to add.
   */
  public void addMesh(Mesh mesh) {
    root.add(mesh);
    addParams();
  }

  public void addMesh(int location, Mesh mesh) {
    root.add(location, mesh);
    addParams();
  }

  public void clear() {
    if (root != null) {
      root.clear();
    }
  }

  public void remove(Mesh m) {
    if (root != null) {
      root.remove(m);
    }
  }

  public void setScale(float _scale) {
    scale = _scale;
  }

  public void setXYScroll(float _dx, float _dy) {
    dx = _dx;
    dy = _dy;
  }

  private void addParams() {
    root.setTextures(textures);
  }
}
