/**
 * Copyright 2010 Per-Erik Bergman (per-erik.bergman@jayway.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.deltaid.sample_utils.ogl;

import android.graphics.Bitmap;
import android.opengl.GLUtils;

import javax.microedition.khronos.opengles.GL10;

/**
 * BitmapPlane is a setup class for Mesh that creates a plane mesh.
 *
 *
 */
public class BitmapPlane extends Mesh {

  private int mTextureId = -1;
  // The bitmap we want to load as a texture.
  private Bitmap mBitmap; // New variable.
  // Indicates if we need to load the texture.
  private boolean mShouldLoadTexture = false; // New variable.
  // Indicates if a texture has ever been loaded from a bitmap
  private boolean mTextureLoaded = false;
  private final float[] mRGBA = {0.5f, 0.6f, 0.5f, 1.0f};
  /**
   * Create a plane with a default with and height of 1 unit.
   */
  private float[] vertices = {
      -1, -1, 0,
       1, -1, 0,
      -1,  1, 0,
       1,  1, 0
  };

  private float textureCoordinates[] = {
      0, 0,
      1, 0,
      0, 1,
      1, 1
  };
  private short[] indices = {0, 1, 2, 1, 3, 2};

  public BitmapPlane(float cameraWidth, float cameraHeight, float width, float height, float ar, float rotation, int txtrIdx) { // width, height in camera coord space
    setCameraSize(cameraWidth, cameraHeight);
    this.ar = ar;
    arFactor = (cameraWidth / cameraHeight) / ar;
    width *= arFactor;
    setRotation(rotation);
    mTextureId = txtrIdx;
    float w = width/camWidth, h = height/camHeight;
    float[] imgVertices = {
        -w, -h, 0,
         w, -h, 0,
        -w,  h, 0,
         w,  h, 0
    };

    setIndices(indices);
    setVertices(imgVertices);
    setTextureCoordinates(textureCoordinates);
  }

  public void draw(GL10 gl) {
    gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[mTextureId]);
    gl.glPushMatrix();
    // Counter-clockwise winding.
    gl.glFrontFace(GL10.GL_CCW);
    // Enable face culling.
    gl.glEnable(GL10.GL_CULL_FACE);
    // What faces to remove with the face culling.
    gl.glCullFace(GL10.GL_BACK);
    // Enabled the vertices buffer for writing and to be used during
    // rendering.
    gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
    // Specifies the location and data format of an array of vertex
    // coordinates to use when rendering.
    gl.glVertexPointer(3, GL10.GL_FLOAT, 0, mVerticesBuffer);
    // Set flat color
    gl.glColor4f(mRGBA[0], mRGBA[1], mRGBA[2], mRGBA[3]);
    gl.glTranslatef(xLoc, yLoc, zLoc);

    if (mShouldLoadTexture) {
      loadGLTexture(gl);
      mShouldLoadTexture = false;
    }
    if (mTextureId != -1 && mTextureBuffer != null) {
      gl.glEnable(GL10.GL_TEXTURE_2D);
      // Enable the texture state
      gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
      // Point to our buffers
      gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, mTextureBuffer);
    }

    rotateContext(gl, rotation);
    // only draw the surface if a texture is available for it
    if (mTextureLoaded && mBitmap != null) {
      gl.glDrawElements(GL10.GL_TRIANGLES, mNumOfIndices, GL10.GL_UNSIGNED_SHORT, mIndicesBuffer);
    }
    if (mTextureId != -1 && mTextureBuffer != null) {
      gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
      gl.glDisable(GL10.GL_TEXTURE_2D);
    }

    // Disable the vertices buffer
    gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);

    // Disable face culling.
    gl.glDisable(GL10.GL_CULL_FACE);
    gl.glPopMatrix();
  }

  /**
   * Set the bitmap to load into a texture.
   *
   * @param bitmap
   */
  public void loadBitmap(Bitmap bitmap) { // New function.
    this.mBitmap = bitmap;
    mShouldLoadTexture = bitmap != null;
  }

  /**
   * Loads the texture.
   *
   * @param gl
   */
  private void loadGLTexture(GL10 gl) { // New function

    // Create Nearest Filtered Texture
    gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER,
        GL10.GL_NEAREST);
    gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER,
        GL10.GL_NEAREST);

    // Different possible texture parameters, e.g. GL10.GL_CLAMP_TO_EDGE
    gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S,
        GL10.GL_CLAMP_TO_EDGE);
    gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T,
        GL10.GL_REPEAT);

    // Use the Android GLUtils to specify a two-dimensional texture image
    // from our bitmap
    GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, mBitmap, 0);
    mTextureLoaded = true;
    //android.util.Log.d("OGL123", GLUtils.getEGLErrorString(gl.glGetError()) + " ondraw if: " + GLUtils.getInternalFormat(mBitmap) + " type: " + GLUtils.getType(mBitmap));
  }
}
