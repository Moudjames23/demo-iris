package com.deltaid.sample_utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by venkynarayanan on 4/2/18.
 */

public class ConfigurationFile {

  private static ArrayList<ImpressionData> enrollImprData = new ArrayList<>();
  private static ArrayList<ImpressionData> identImprData = new ArrayList<>();
  private static ArrayList<ImpressionData> capImprData = new ArrayList<>();
  private static int[] ledList = null;
  private static HashSet<String> devClasses = new HashSet<>();
  private static String defaultDevClass;
  static {
    String productCode = android.os.Build.PRODUCT;
    productCode = productCode.replace("-", "_"); // "-" is not acceptable in Java classnames, swap with "_"
    defaultDevClass = "_"+ productCode;
  }


  private static void setDeafultCaptureImpressions() {
    ImpressionData idata = new ImpressionData();
    idata.imprId.add("_0");
    idata.led.add(1);
    idata.expMult.add(100);
    idata.deviceClassName = defaultDevClass;
    capImprData.clear();
    addImprData("C", idata);
  }

  private static void setDeafultEnrollImpressions() {
    ImpressionData idata = new ImpressionData();
    idata.imprId.add("_0");
    idata.led.add(1);
    idata.expMult.add(100);
    idata.deviceClassName = defaultDevClass;
    enrollImprData.clear();
    addImprData("E", idata);
  }

  private static void setDeafultIdentImpressions() {
    ImpressionData idata = new ImpressionData();
    idata.imprId.add("_0");
    idata.led.add(1);
    idata.expMult.add(100);
    idata.deviceClassName = defaultDevClass;
    identImprData.clear();
    addImprData("I", idata);
  }

  private static void setDefaultImpressions() {
    setDeafultCaptureImpressions();
    setDeafultEnrollImpressions();
    setDeafultIdentImpressions();
  }

  private static void addImprData(String op, ImpressionData idata) {
    if (ledList == null && idata.led != null && idata.led.size() > 0) {
      ledList  = new int[idata.led.size()];
      for (int i = 0; i < idata.led.size(); i++) {
        ledList[i] = idata.led.get(i);
      }
    }
    if (op.equals("E")) {
      enrollImprData.add(idata);
    } else if (op.equals("I")) {
      identImprData.add(idata);
    } else if (op.equals("C")) {
      capImprData.add(idata);
    }
  }

  public static boolean readImpressionConfigFile(File configFile) {
    enrollImprData.clear();
    identImprData.clear();
    capImprData.clear();
    ledList = null;
    if (configFile == null || !configFile.isFile()) {
      setDefaultImpressions();
      return true;
    }

    try {
      BufferedReader br = new BufferedReader(new FileReader(configFile));
      String line;
      int prevDistance = -1;
      float prevPreviewZoom = -1.0f;
      String prevDevClass = null;
      String prevOp = "";
      ImpressionData idata = null;
      String op = "";
      while ((line = br.readLine()) != null) {
        if (line.startsWith("//") || line.contains("=")) {
          continue;
        }
        String kv[] = line.split("\\s+");
        if (kv.length < 3 || kv.length > 7) {
          continue; // invalid line, too few values or too many values
        }
        int ledNum = 0, distance = -1;
        String id = null, devClass = null;
        int expMult = 100;
        float previewZoom = 1.0f;
        op = kv[0];
        if (!op.equals("E") && !op.equals("I") && !op.equals("C")) {
          continue;
        }
        switch (kv.length) {
          case 7:
            previewZoom = Float.parseFloat(kv[6]);
          case 6:
            devClass = kv[5];
          case 5:
            distance = Integer.parseInt(kv[4]);
          case 4:
            id = kv[1];
            ledNum = Integer.parseInt(kv[2]);
            expMult = Integer.parseInt(kv[3]);
            if (devClass == null) {
              devClass = defaultDevClass;
            }
            if (distance == -1) {
              distance = 0;
            }
            break;
        }
        if (prevDistance == -1 && prevDevClass == null && prevOp.length() == 0 && prevPreviewZoom == -1.0f) {
          idata = new ImpressionData();
        } else if (prevDistance != distance || !prevDevClass.equals(devClass) || !op.equals(prevOp) || prevPreviewZoom != previewZoom) {
          if (!op.equals(prevOp)) {
            addImprData(prevOp, idata);
          } else {
            addImprData(op, idata);
          }
          //imprData.add(idata);
          idata = new ImpressionData();
        }
        devClasses.add(devClass);
        idata.deviceClassName = devClass;
        idata.distance = distance;
        idata.imprId.add(id);
        idata.led.add(ledNum);
        idata.expMult.add(expMult);
        idata.previewZoom = previewZoom;
        prevDistance = distance;
        prevDevClass = devClass;
        prevOp = op;
        prevPreviewZoom = previewZoom;
      }
      if (idata != null) {
        addImprData(op, idata);
        //imprData.add(idata);
      }
      br.close();
    } catch (IOException e) {
      e.printStackTrace();
      //Toast.makeText(this, "Error reading Collections config file. Using default values.", Toast.LENGTH_SHORT).show();
      //imprData = null;
      enrollImprData.clear();
      identImprData.clear();
      capImprData.clear();
    }
    if (capImprData.size() == 0) {
      setDeafultCaptureImpressions();
    }
    if (enrollImprData.size() == 0) {
      setDeafultEnrollImpressions();
    }
    if (identImprData.size() == 0) {
      setDeafultIdentImpressions();
    }
    return true;
  }

  public static ArrayList<ImpressionData> getEnrollmentImprData() {
    return enrollImprData;
  }

  public static ArrayList<ImpressionData> getIdentificationImprData() {
    return identImprData;
  }

  public static ArrayList<ImpressionData> getCaptureImprData() {
    return capImprData;
  }

  public static int[] getLedList() {
    return ledList;
  }

  public static HashSet<String> getDevClasses() {
    if (devClasses.size() == 0) {
      devClasses.add(defaultDevClass);
    }
    return devClasses;
  }
}
