package com.deltaid.sample_utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.Toast;

abstract public class CheckNetworkPermissionsActivity extends Activity {
  private static int REQUEST_PERMISSION_CODE = 1;
  private int indexPermissionNetworkState;
  private int indexPermissionReceiveInternet;
  private boolean requestNetworkStatePermission;
  private boolean requestInternetPermission;
  private int numPermissionsToRequest;
  private boolean hasNetworkStatePermission;
  private boolean hasInternetPermission;
  private boolean permissionDenied;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public void onResume() {
    super.onResume();
    if (!permissionDenied) {
      numPermissionsToRequest = 0;
      checkPermissions();
    } else {
      permissionDenied = false;
    }
  }

  private void checkPermissions() {
    checkNetworkStatePermission();
    checkInternetPermission();
    if (numPermissionsToRequest != 0) {
      buildPermissionsRequest();
    } else {
      handleSuccess();
    }
  }

  @TargetApi(23)
  private void checkNetworkStatePermission() {
    if (checkSelfPermission(Manifest.permission.ACCESS_NETWORK_STATE)
        != PackageManager.PERMISSION_GRANTED) {
      numPermissionsToRequest++;
      requestNetworkStatePermission = true;
    } else {
      hasNetworkStatePermission = true;
    }
  }

  @TargetApi(23)
  private void checkInternetPermission() {
    if (checkSelfPermission(Manifest.permission.INTERNET)
        != PackageManager.PERMISSION_GRANTED) {
      numPermissionsToRequest++;
      requestInternetPermission = true;
    } else {
      hasInternetPermission = true;
    }
  }

  @TargetApi(23)
  private void buildPermissionsRequest() {
    String[] permissionsToRequest = new String[numPermissionsToRequest];
    int permissionsRequestIndex = 0;

    if (requestNetworkStatePermission) {
      permissionsToRequest[permissionsRequestIndex] = Manifest.permission.ACCESS_NETWORK_STATE;
      indexPermissionNetworkState = permissionsRequestIndex;
      permissionsRequestIndex++;
    }

    if (requestInternetPermission) {
      permissionsToRequest[permissionsRequestIndex] =
          Manifest.permission.INTERNET;
      indexPermissionReceiveInternet = permissionsRequestIndex;
      permissionsRequestIndex++;
    }

    requestPermissions(permissionsToRequest, REQUEST_PERMISSION_CODE);
  }

  @Override
  public void onRequestPermissionsResult(int requestCode,
                                         String permissions[], int[] grantResults) {

    if (requestNetworkStatePermission) {
      if ((grantResults.length >= indexPermissionNetworkState + 1) &&
          (grantResults[indexPermissionNetworkState] ==
              PackageManager.PERMISSION_GRANTED)) {
        hasNetworkStatePermission = true;
      } else {
        permissionDenied = true;
      }
    }
    if (requestInternetPermission) {
      if ((grantResults.length >= indexPermissionReceiveInternet + 1) &&
          (grantResults[indexPermissionReceiveInternet] ==
              PackageManager.PERMISSION_GRANTED)) {
        hasInternetPermission = true;
      } else {
        permissionDenied = true;
      }
    }

    if (hasNetworkStatePermission && hasInternetPermission) {
      handleSuccess();
    } else if (permissionDenied) {
      handleFailure();
    }
  }

  private void handleSuccess() {
    startNewActivity();
    finish();
  }
  private void handleFailure() {
    if (!hasNetworkStatePermission || !hasInternetPermission) {
      Toast.makeText(this, getAppName() + " requires permission to make and manage phone calls. Go to Settings->Apps->" + "getAppName()" + "->Permissions set \"Network\" switch to on.", Toast.LENGTH_LONG).show();
    }
  }

  abstract protected void startNewActivity();
  abstract protected String getAppName();
}
