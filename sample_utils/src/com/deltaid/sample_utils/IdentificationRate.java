package com.deltaid.sample_utils;

import android.util.Pair;

import java.util.ArrayList;

public class IdentificationRate {
  private final Long registerStartTime = 0250L; // start of statistics collection [ms]
  private final Long registerEndTime   = 3250L; // end of statistics collection [ms]
  private Long resetTime; // time at reset [ms]
  private ArrayList<Pair<Long, Integer>> timeScorePairs; // future-proofing, not fully used now

  public IdentificationRate() {
    reset();
  }

  public void reset() {
    resetTime = System.currentTimeMillis();
    timeScorePairs = new ArrayList< Pair<Long, Integer> >();
  }

  public void register(int score) {
    final Long scoreTime = System.currentTimeMillis() - resetTime;

    if(scoreTime >= registerStartTime && scoreTime <= registerEndTime) {
      timeScorePairs.add(new Pair<Long, Integer>(scoreTime, score));
    }
  }

  // score sampling period in seconds
  private double samplingPeriod() {
    return (double)(registerEndTime-registerStartTime)/1000f;
  }

  // number of score at or above a given threshold
  public int scoresAtOrAbove(int scoreThreshold) {
    int scoreCount = 0;
    for(int i = 0; i < timeScorePairs.size(); ++i) {
      if (timeScorePairs.get(i).second >= scoreThreshold) {
        ++scoreCount;
      }
    }

    return scoreCount;
  }

  // time in ms of first score at or above a given threshold, 0 is returned when no matches exist
  public Long timeofFirstScoreAtOrAbove(int scoreThreshold) {
    for(int i = 0; i < timeScorePairs.size(); ++i) {
      if (timeScorePairs.get(i).second >= scoreThreshold) {
        return timeScorePairs.get(i).first;
      }
    }

    return 0L;
  }

  // true if samples are still being accepted
  public boolean samplingActive() {
    return System.currentTimeMillis() - resetTime <= registerEndTime;
  }

  // text-based result string for a single security level (without leading zero for rates below one)
  public String result(int minimumScoreLevel, boolean showTentativeRateDuringSampling) {
    final Long timeSinceReset = System.currentTimeMillis() - resetTime;
    final boolean stillSampling =  timeSinceReset <= registerEndTime;

    if (!showTentativeRateDuringSampling && stillSampling) {
      return "";
    }

    final double rate = (double)(scoresAtOrAbove(minimumScoreLevel)) /
                                (stillSampling ? timeSinceReset/1000f : samplingPeriod());
    final String fmtStr = rate > .0 && rate < .95 ? "%.1f" : "%.0f";
    final String decoratedFmtStr = stillSampling ? "(" + fmtStr + ")" : fmtStr;

    return String.format(decoratedFmtStr, rate).replaceAll( "^0(\\..*)$", "$1" );
  }
}
